var elixir = require('laravel-elixir');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Less
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir.config.sourcemaps = false;

var paths = {
	src : {
		less   : './resources/assets/less/',
		js     : './resources/assets/js/',
		vendor : './resources/assets/vendor/',
		fonts  : './resources/assets/fonts/'
	},
	output : {
		// Instalação servidor Trupe
		less  : '../public_html/previa-suavipan/assets/css/',
		js    : '../public_html/previa-suavipan/assets/js/',
		fonts : '../public_html/previa-suavipan/assets/fonts/'

		// Produção
		less  : '../web/previa/assets/css/',
		js    : '../web/previa/assets/js/',
		fonts : '../web/previa/assets/fonts/'
	}
};

elixir(function(mix) {
    mix
    	// --------------------------------//
    	// Cópia de arquivos originais da
    	// pasta Vendor para o acesso público
    	// --------------------------------//
    	.copy( paths.src.vendor + 'modernizr/modernizr.js', paths.output.js + 'modernizr.js', './')
    	.copy( paths.src.vendor + 'jquery/dist/jquery.min.js', paths.output.js + 'jquery.js', './')
    	.copy( paths.src.vendor + 'bootstrap/dist/js/bootstrap.min.js', paths.output.js + 'bootstrap.js', './')
      .copy( paths.src.vendor + 'bootstrap/dist/fonts/', paths.output.fonts)
      .copy( paths.src.vendor + 'ckeditor/', paths.output.js + 'ckeditor/')

      .copy( paths.src.vendor + 'fancybox/source/fancybox_loading.gif', paths.output.less)
      .copy( paths.src.vendor + 'fancybox/source/fancybox_sprite.png', paths.output.less)
      .copy( paths.src.vendor + 'fancybox/source/fancybox_overlay.png', paths.output.less)

      // Executar somente 1 vez \/
      .copy( paths.src.vendor + 'ckeditor/config.js', paths.src.js + 'ckeditor_config.js')


    	// --------------------------------//
    	// Estilos e Scripts do Site
    	// --------------------------------//


			.less([
				paths.src.less + 'base.less',
				paths.src.less + 'home.less',
				paths.src.less + 'todos-produtos.less',
				paths.src.less + 'nossas-linhas.less',
				paths.src.less + 'a-suavipan.less',
				paths.src.less + 'contato.less'
			], paths.output.less + 'site.css')

    	.styles([
				paths.src.vendor + 'reset-css/reset.css',
        paths.src.vendor + 'fancybox/source/jquery.fancybox.css'
    	], paths.output.less + 'vendor.css', './')

    	.scripts([
        paths.src.vendor + 'fancybox/source/jquery.fancybox.js',
				paths.src.vendor + 'jquery-cycle2/build/jquery.cycle2.js',
				//paths.src.vendor + 'easytabs/vendor/jquery.hashchange.min.js',
				paths.src.vendor + 'easytabs/lib/jquery.easytabs.js',
        paths.src.js + 'site.js'
      ], paths.output.js + 'site.js', './')


    	// --------------------------------//
    	// Estilos e Scripts do Painel
    	// --------------------------------//

    	.less( paths.src.less + 'painel.less', paths.src.less)

        // ESTILOS
    	.styles([
    	  paths.src.vendor + 'reset-css/reset.css',
				paths.src.vendor + 'jquery-ui/themes/base/jquery-ui.css',
				paths.src.vendor + 'bootstrap/dist/css/bootstrap.min.css',
				paths.src.vendor + 'x-editable/dist/bootstrap3-editable/css/bootstrap-editable.css',
				paths.src.less   + 'painel.css',
    	], paths.output.less + 'painel.css', './')

      // SCRIPTS
      .scripts( paths.src.js + 'ckeditor_config.js', paths.output.js + 'ckeditor_config.js', './')

      .scripts([
        paths.src.vendor + 'jquery-ui/jquery-ui.min.js',
        paths.src.vendor + 'bootbox/bootbox.js',
        paths.src.vendor + 'bootstrap/dist/js/bootstrap.min.js',
        paths.src.vendor + 'blueimp-file-upload/js/jquery.fileupload.js',
				paths.src.vendor + 'x-editable/dist/bootstrap3-editable/js/bootstrap-editable.js',
				paths.src.js     + 'painel.js',
				paths.src.js     + 'painel-upload-imagens.js'
      ], paths.output.js  + 'painel.js', './');

});
