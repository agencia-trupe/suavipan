<?php

namespace Suavipan\Models;

use Illuminate\Database\Eloquent\Model;

class Conteudo extends Model
{

  /**
   * The database table used by the model.
   *
   * @var string
   */
  protected $table = 'conteudos';

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
    '',
  ];

  /**
   * The attributes excluded from the model's JSON form.
   *
   * @var array
   */
  protected $hidden = ['id'];

  public function scopeIndice($query, $indice)
  {
    return $query->where('indice', $indice);
  }
}
