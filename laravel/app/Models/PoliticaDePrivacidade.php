<?php

namespace Suavipan\Models;

use Illuminate\Database\Eloquent\Model;

class PoliticaDePrivacidade extends Model
{
    protected $table = 'politica_de_privacidade';

    protected $guarded = ['id'];
}
