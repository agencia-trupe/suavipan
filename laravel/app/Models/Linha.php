<?php

namespace Suavipan\Models;

use App;
use Illuminate\Database\Eloquent\Model;

class Linha extends Model
{

  /**
   * The database table used by the model.
   *
   * @var string
   */
  protected $table = 'linhas';

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
    '',
  ];

  /**
   * The attributes excluded from the model's JSON form.
   *
   * @var array
   */
  protected $hidden = ['id'];

  public function traduz($campo)
  {
    return App::getLocale() == 'pt-br' ? $this->{$campo.'_pt'} : $this->{$campo.'_'.App::getLocale()};
  }

  public function scopeOrdenacaoNormal($query)
  {
    return $query->orderby('id', 'asc');
  }

  public function scopeFindBySlug($query, $slug)
  {
    return $query->where('slug_pt', $slug)->first();
  }

  public function tipos()
  {
    return $this->hasMany('Suavipan\Models\Tipo', 'linhas_id')->orderBy('ordem', 'asc');
  }

  public function produtos()
  {
    return $this->hasMany('Suavipan\Models\Produto', 'linhas_id')->orderBy('titulo_pt', 'asc');
  }

}
