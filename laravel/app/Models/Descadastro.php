<?php

namespace Suavipan\Models;

use Illuminate\Database\Eloquent\Model;

class Descadastro extends Model
{
    protected $table = 'descadastros';

    protected $guarded = ['id'];
}
