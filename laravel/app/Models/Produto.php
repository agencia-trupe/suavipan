<?php

namespace Suavipan\Models;

use App;
use Illuminate\Database\Eloquent\Model;

class Produto extends Model
{

  /**
   * The database table used by the model.
   *
   * @var string
   */
  protected $table = 'produtos';

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
    '',
  ];

  /**
   * The attributes excluded from the model's JSON form.
   *
   * @var array
   */
  protected $hidden = ['id'];

  public function traduz($campo)
  {
    return App::getLocale() == 'pt-br' ? $this->{$campo.'_pt'} : $this->{$campo.'_'.App::getLocale()};
  }

  public function scopeOrdenacaoNormal($query)
  {
    return $query->orderby('id', 'asc');
  }

  public function scopeOrdenado($query)
  {
    return $query->orderby('ordem', 'asc');
  }

  public function scopeFindBySlug($query, $slug)
  {
    return $query->where('slug_pt', $slug)->first();
  }

  public function scopeBusca($query, $termo)
  {
    return $query->where('titulo_pt', 'like', '%'.$termo.'%')
                 ->orWhere('titulo_en', 'like', '%'.$termo.'%')
                 ->orWhere('titulo_es', 'like', '%'.$termo.'%')
                 ->orWhere('ingredientes_es', 'like', '%'.$termo.'%')
                 ->orWhere('ingredientes_es', 'like', '%'.$termo.'%')
                 ->orWhere('ingredientes_es', 'like', '%'.$termo.'%')
                 ->orWhereHas('tipo', function($query) use($termo){
                   $query->where('titulo_pt', 'like', '%'.$termo.'%')
                        ->orWhere('titulo_en', 'like', '%'.$termo.'%')
                        ->orWhere('titulo_es', 'like', '%'.$termo.'%');
                 });
  }

  public function linha()
  {
    return $this->belongsTo('Suavipan\Models\Linha', 'linhas_id');
  }

  public function tipo()
  {
    return $this->belongsTo('Suavipan\Models\Tipo', 'tipos_id');
  }

}
