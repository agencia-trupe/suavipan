<?php

namespace Suavipan\Http\Controllers\Site\ASuavipan;

use Illuminate\Http\Request;
use Suavipan\Http\Controllers\Controller;

class ASuavipanController extends Controller
{

  public function getIndex(Request $request)
  {
    return view('site.a-suavipan.index');
  }

}
