<?php

namespace Suavipan\Http\Controllers\Site\Home;

use Illuminate\Http\Request;
use Suavipan\Http\Controllers\Controller;

use View;

use Suavipan\Models\Linha;
use Suavipan\Models\CadastroNewsletter;

class HomeController extends Controller
{

  public function getIndex(Request $request)
  {
    return view('site.home.index');
  }

  public function postCadastroNewsletter(Request $request)
  {
    $this->validate($request, [
      'newsletter.nome'  => 'required',
      'newsletter.email' => 'required|email'
    ]);

    $cadastro = CadastroNewsletter::create($request->newsletter);

    $request->session()->flash('newsletter_sucesso', true);
    return redirect()->back();
  }

}
