<?php

namespace Suavipan\Http\Controllers\Site\NossasLinhas;

use Illuminate\Http\Request;
use Suavipan\Http\Controllers\Controller;

use View;

use Suavipan\Models\Linha;
use Suavipan\Models\Tipo;
use Suavipan\Models\Produto;

class NossasLinhasController extends Controller
{

  public function getIndex(Request $request, $slug_linha = '', $slug_tipo = '')
  {
    $listaLinhas = Linha::all();

    if($slug_linha){
      $linhaSelecionada = Linha::findBySlug($slug_linha);
    }else{
      $linhaSelecionada = null;
    }

    if($linhaSelecionada && $slug_tipo){
      $tipoSelecionado = Tipo::where('linhas_id', $linhaSelecionada->id)->findBySlug($slug_tipo);
    }else{
      $tipoSelecionado = null;
    }

    return view('site.nossas-linhas.index', compact(
      'listaLinhas',
      'linhaSelecionada',
      'tipoSelecionado'
    ));
  }

}
