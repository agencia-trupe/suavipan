<?php

namespace Suavipan\Http\Controllers\Painel;

use Illuminate\Http\Request;

use Suavipan\Http\Requests;
use Suavipan\Http\Controllers\Controller;
use Suavipan\Models\AceiteDeCookies;

class AceiteDeCookiesController extends Controller
{
    public function index()
    {
        $registros = AceiteDeCookies::orderBy('created_at', 'DESC')->get();

        return view('painel.cookies.index', compact('registros'));
    }
}
