<?php

namespace Suavipan\Http\Controllers\Painel;

use Illuminate\Http\Request;

use Suavipan\Http\Requests;
use Suavipan\Http\Controllers\Controller;
use Suavipan\Http\Requests\PoliticaDePrivacidadeRequest;
use Suavipan\Models\PoliticaDePrivacidade;

class PoliticaDePrivacidadeController extends Controller
{
    public function index()
    {
        $registro = PoliticaDePrivacidade::first();

        return view('painel.politica-de-privacidade.edit', compact('registro'));
    }

    public function update(PoliticaDePrivacidadeRequest $request, $registro_id)
    {
        try {
            $input = $request->all();
            
            $registro = PoliticaDePrivacidade::where('id', $registro_id)->first();

            $registro->update($input);

            return redirect()->route('painel.politica-de-privacidade.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {
            return back()->withErrors(['Erro ao alterar registro: ' . $e->getMessage()]);
        }
    }
}
