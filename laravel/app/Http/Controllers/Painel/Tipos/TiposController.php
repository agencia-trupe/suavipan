<?php

namespace Suavipan\Http\Controllers\Painel\Tipos;

use Illuminate\Http\Request;
use Suavipan\Http\Controllers\Controller;

use Suavipan\Libs\Thumbs;
use Suavipan\Models\Tipo;
use Suavipan\Models\Linha;

class TiposController extends Controller{

	public function index(Request $request)
	{
    $linha = Linha::findOrFail($request->linha_id);
		return view('painel.tipos.index')->with(compact('linha'));
	}
	
	public function create(Request $request)
	{
	   $linha = Linha::findOrFail($request->linha_id);
	
		return view('painel.tipos.create')->with(compact('linha'));
	}
	
	public function store(Request $request)
	{
		$this->validate($request, [
			'titulo_pt' => 'required',
			'imagem'    => 'sometimes|image'
  	]);

		$object = new Tipo;
		
		$object->linhas_id = $request->linhas_id;

		$object->titulo_pt = $request->titulo_pt;
		$object->titulo_en = $request->titulo_en;
		$object->titulo_es = $request->titulo_es;

		$object->slug_pt = str_slug($request->titulo_pt);
		$object->slug_en = str_slug($request->titulo_en);
		$object->slug_es = str_slug($request->titulo_es);

		$object->chamada_pt = $request->chamada_pt;
		$object->chamada_en = $request->chamada_en;
		$object->chamada_es = $request->chamada_es;

		$object->cor_fundo = $request->cor_fundo;

		$imagem = Thumbs::make($request, 'imagem', null, null, 'tipos/');
		if($imagem) $object->imagem = $imagem;

		try {

			$object->save();

			$request->session()->flash('sucesso', 'Tipo criado com sucesso.');

			return redirect()->route('painel.tipos.index', ['linha_id' => $object->linhas_id]);

		} catch (\Exception $e) {

			$request->flash();

			return back()->withErrors(array('Erro ao criar Tipo! ('.$e->getMessage().')'));

		}
	}
	

	public function edit($id)
	{
		$registro = Tipo::find($id);
		return view('painel.tipos.edit')->with(compact('registro'));
	}

	public function update(Request $request, $id)
	{
		$this->validate($request, [
			'titulo_pt' => 'required',
			'imagem'    => 'sometimes|image'
  	]);

		$object = Tipo::find($id);

		$object->titulo_pt = $request->titulo_pt;
		$object->titulo_en = $request->titulo_en;
		$object->titulo_es = $request->titulo_es;

		$object->slug_pt = str_slug($request->titulo_pt);
		$object->slug_en = str_slug($request->titulo_en);
		$object->slug_es = str_slug($request->titulo_es);

		$object->chamada_pt = $request->chamada_pt;
		$object->chamada_en = $request->chamada_en;
		$object->chamada_es = $request->chamada_es;

		$object->cor_fundo = $request->cor_fundo;

		$imagem = Thumbs::make($request, 'imagem', null, null, 'tipos/');
		if($imagem) $object->imagem = $imagem;

		try {

			$object->save();

			$request->session()->flash('sucesso', 'Tipo alterado com sucesso.');

			return redirect()->route('painel.tipos.index', ['linha_id' => $object->linhas_id]);

		} catch (\Exception $e) {

			$request->flash();

			return back()->withErrors(array('Erro ao alterar Tipo! ('.$e->getMessage().')'));

		}
	}

}
