<?php

namespace Suavipan\Http\Controllers\Painel\Cadastros;

use Illuminate\Http\Request;

use Suavipan\Http\Requests;
use Suavipan\Http\Controllers\Controller;
use Suavipan\Models\Descadastro;

class DescadastrosController extends Controller
{
    public function index(Request $request)
    {
        return view('painel.descadastros.index')->with(['registros' => Descadastro::all()]);
    }

    public function getDownload(Request $request)
    {
        $registros = Descadastro::all();

        $filename = 'Descadastros_' . date('d_m_Y');

        $registrosArray = [];

        foreach ($registros as $key => $value) {
            $registrosArray[$key]['email'] = $value->email;
            $registrosArray[$key]['data de cadastro'] = $value->created_at->format('d/m/Y H:i');
        }

        \Excel::create($filename, function ($excel) use ($registrosArray) {
            $excel->sheet('Relatório de Descadastros', function ($sheet) use ($registrosArray) {
                $sheet->fromArray($registrosArray);
            });
        })->download('xls');
    }

    public function destroy(Request $request, $id)
    {
        $object = Descadastro::find($id);
        $object->delete();

        $request->session()->flash('sucesso', 'Cadastro removido com sucesso.');

        return redirect()->route('painel.descadastros.index');
    }
}
