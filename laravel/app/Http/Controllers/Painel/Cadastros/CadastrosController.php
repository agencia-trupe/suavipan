<?php

namespace Suavipan\Http\Controllers\Painel\Cadastros;

use Illuminate\Http\Request;
use Suavipan\Http\Controllers\Controller;

use Suavipan\Models\CadastroNewsletter;

class CadastrosController extends Controller{

	public function index(Request $request)
	{
    return view('painel.cadastros.index')->with(['registros' => CadastroNewsletter::all()]);
	}

  public function getDownload(Request $request)
  {
    $registros = CadastroNewsletter::all();

    $filename = 'Cadastros_Newsletter_'.date('d_m_Y');

    $registrosArray = [];

    foreach ($registros as $key => $value) {
      $registrosArray[$key]['nome'] = $value->nome;
      $registrosArray[$key]['email'] = $value->email;
      $registrosArray[$key]['data de cadastro'] = $value->created_at->format('d/m/Y H:i');
    }

    \Excel::create($filename, function($excel) use($registrosArray) {
	    $excel->sheet('Relatório de Cadastros', function($sheet) use ($registrosArray) {
	      $sheet->fromArray($registrosArray);
	    });
		})->download('xls');
  }

  public function destroy(Request $request, $id)
	{
		$object = CadastroNewsletter::find($id);
		$object->delete();

		$request->session()->flash('sucesso', 'Cadastro removido com sucesso.');

		return redirect()->route('painel.cadastros.index');
	}

}
