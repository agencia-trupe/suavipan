<?php

namespace Suavipan\Http\Controllers\Painel\Conteudos;

use Illuminate\Http\Request;
use Suavipan\Http\Controllers\Controller;
use Suavipan\Models\Conteudo;

class ConteudosController extends Controller
{
	public function indexHome()
	{
		$registros = Conteudo::all()->take(20);

		return view('painel.conteudos.home', compact('registros'));
	}

	public function updateHome(Request $request)
	{
		$conteudos = $request->conteudos;
		foreach ($conteudos as $indice => $valores) {
			foreach ($valores as $campo => $valor) {
				$conteudo = Conteudo::indice($indice)->first();
				$conteudo->{$campo} = $valor;
				$conteudo->save();
			}
		}

		$arquivos = $request->arquivos;

		if (isset($arquivos)) {
			foreach ($arquivos as $indice => $imagem) {
				if ($imagem) {
					$filename = $imagem->getClientOriginalName();
					$conteudo = Conteudo::indice($indice)->first();
					$imagem->move('assets/img/conteudos', $filename);
					$conteudo->valor_pt_br = $filename;
					$conteudo->save();
				}
			}
		}

		try {
			$request->session()->flash('sucesso', 'Conteúdo alterado com sucesso.');

			return redirect()->route('painel.home.index');
		} catch (\Exception $e) {
			$request->flash();

			return back()->withErrors(array('Erro ao alterar conteúdo! (' . $e->getMessage() . ')'));
		}
	}

	public function indexProdutos()
	{
		$registros = Conteudo::all()->slice(20, 4);

		return view('painel.conteudos.todos-os-produtos', compact('registros'));
	}

	public function updateProdutos(Request $request)
	{
		$conteudos = $request->conteudos;
		foreach ($conteudos as $indice => $valores) {
			foreach ($valores as $campo => $valor) {
				$conteudo = Conteudo::indice($indice)->first();
				$conteudo->{$campo} = $valor;
				$conteudo->save();
			}
		}

		$arquivos = $request->arquivos;

		if (isset($arquivos)) {
			foreach ($arquivos as $indice => $imagem) {
				if ($imagem) {
					$filename = $imagem->getClientOriginalName();
					$conteudo = Conteudo::indice($indice)->first();
					$imagem->move('assets/img/conteudos', $filename);
					$conteudo->valor_pt_br = $filename;
					$conteudo->save();
				}
			}
		}

		try {
			$request->session()->flash('sucesso', 'Conteúdo alterado com sucesso.');

			return redirect()->route('painel.todos-os-produtos.index');
		} catch (\Exception $e) {
			$request->flash();

			return back()->withErrors(array('Erro ao alterar conteúdo! (' . $e->getMessage() . ')'));
		}
	}

	public function indexLinhas()
	{
		$registros = Conteudo::all()->slice(24, 11);

		return view('painel.conteudos.nossas-linhas', compact('registros'));
	}

	public function updateLinhas(Request $request)
	{
		$conteudos = $request->conteudos;
		foreach ($conteudos as $indice => $valores) {
			foreach ($valores as $campo => $valor) {
				$conteudo = Conteudo::indice($indice)->first();
				$conteudo->{$campo} = $valor;
				$conteudo->save();
			}
		}

		$arquivos = $request->arquivos;

		if (isset($arquivos)) {
			foreach ($arquivos as $indice => $imagem) {
				if ($imagem) {
					$filename = $imagem->getClientOriginalName();
					$conteudo = Conteudo::indice($indice)->first();
					$imagem->move('assets/img/conteudos', $filename);
					$conteudo->valor_pt_br = $filename;
					$conteudo->save();
				}
			}
		}

		try {
			$request->session()->flash('sucesso', 'Conteúdo alterado com sucesso.');

			return redirect()->route('painel.nossas-linhas.index');
		} catch (\Exception $e) {
			$request->flash();

			return back()->withErrors(array('Erro ao alterar conteúdo! (' . $e->getMessage() . ')'));
		}
	}

	public function indexSuavipan()
	{
		$registros = Conteudo::all()->slice(35, 5);

		return view('painel.conteudos.a-suavipan', compact('registros'));
	}

	public function updateSuavipan(Request $request)
	{
		$conteudos = $request->conteudos;
		foreach ($conteudos as $indice => $valores) {
			foreach ($valores as $campo => $valor) {
				$conteudo = Conteudo::indice($indice)->first();
				$conteudo->{$campo} = $valor;
				$conteudo->save();
			}
		}

		$arquivos = $request->arquivos;

		if (isset($arquivos)) {
			foreach ($arquivos as $indice => $imagem) {
				if ($imagem) {
					$filename = $imagem->getClientOriginalName();
					$conteudo = Conteudo::indice($indice)->first();
					$imagem->move('assets/img/conteudos', $filename);
					$conteudo->valor_pt_br = $filename;
					$conteudo->save();
				}
			}
		}

		try {
			$request->session()->flash('sucesso', 'Conteúdo alterado com sucesso.');

			return redirect()->route('painel.a-suavipan.index');
		} catch (\Exception $e) {
			$request->flash();

			return back()->withErrors(array('Erro ao alterar conteúdo! (' . $e->getMessage() . ')'));
		}
	}

	public function indexContato()
	{
		$registros = Conteudo::all()->slice(40, 10);

		return view('painel.conteudos.contato', compact('registros'));
	}

	public function updateContato(Request $request)
	{
		$conteudos = $request->conteudos;
		foreach ($conteudos as $indice => $valores) {
			foreach ($valores as $campo => $valor) {
				$conteudo = Conteudo::indice($indice)->first();
				$conteudo->{$campo} = $valor;
				$conteudo->save();
			}
		}

		$arquivos = $request->arquivos;

		if (isset($arquivos)) {
			foreach ($arquivos as $indice => $imagem) {
				if ($imagem) {
					$filename = $imagem->getClientOriginalName();
					$conteudo = Conteudo::indice($indice)->first();
					$imagem->move('assets/img/conteudos', $filename);
					$conteudo->valor_pt_br = $filename;
					$conteudo->save();
				}
			}
		}

		try {
			$request->session()->flash('sucesso', 'Conteúdo alterado com sucesso.');

			return redirect()->route('painel.contato.index');
		} catch (\Exception $e) {
			$request->flash();

			return back()->withErrors(array('Erro ao alterar conteúdo! (' . $e->getMessage() . ')'));
		}
	}
}
