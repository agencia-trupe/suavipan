<?php

namespace Suavipan\Http\Controllers\Site\Home;

use Illuminate\Http\Request;
use Suavipan\Http\Controllers\Controller;
use Suavipan\Models\AceiteDeCookies;
use View;

use Suavipan\Models\Linha;
use Suavipan\Models\CadastroNewsletter;

class HomeController extends Controller
{

  public function getIndex(Request $request)
  {
    $verificacao = AceiteDeCookies::where('ip', $request->ip())->first();

    return view('site.home.index', compact('verificacao'));
  }

  public function getIndexTemp(Request $request)
  {
    return view('site.home.indexTemp');
  }

  public function postCadastroNewsletter(Request $request)
  {
    $this->validate($request, [
      'newsletter.nome'  => 'required',
      'newsletter.email' => 'required|email',
      'optIn'  => 'accepted'
    ]);

    $cadastro = CadastroNewsletter::create($request->newsletter);
    
    $request->session()->flash('newsletter_sucesso', true);
    return redirect()->back();
  }

  public function postCookies(Request $request)
  {
    $input = $request->all();
    $input['ip'] = $request->ip();

    AceiteDeCookies::create($input);

    return redirect('/');
  }
}
