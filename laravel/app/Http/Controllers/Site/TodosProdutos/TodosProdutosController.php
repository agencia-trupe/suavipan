<?php

namespace Suavipan\Http\Controllers\Site\TodosProdutos;

use Illuminate\Http\Request;
use Suavipan\Http\Controllers\Controller;

use View;

use Suavipan\Models\Linha;
use Suavipan\Models\Produto;

class TodosProdutosController extends Controller
{

  public function getIndex(Request $request)
  {
    $listaLinhas = Linha::all();
    return view('site.todos-produtos.index', compact('listaLinhas'));
  }

  public function postBusca(Request $request)
  {
    return view('site.todos-produtos.busca', [
      'resultados' => Produto::busca($request->termo)->get(),
      'termo'      => $request->termo
    ]);
  }

}
