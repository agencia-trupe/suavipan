<?php

namespace Suavipan\Http\Controllers\Site;

use Illuminate\Http\Request;

use Suavipan\Http\Requests;
use Suavipan\Http\Controllers\Controller;
use Suavipan\Models\PoliticaDePrivacidade;

class PoliticaDePrivacidadeController extends Controller
{
    public function index()
    {
        $politica = PoliticaDePrivacidade::first();

        return view('site.politica-de-privacidade.index', compact('politica'));
    }
}
