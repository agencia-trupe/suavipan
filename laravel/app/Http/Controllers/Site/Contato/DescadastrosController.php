<?php

namespace Suavipan\Http\Controllers\Site\Contato;

use Illuminate\Http\Request;

use Suavipan\Http\Controllers\Controller;
use Suavipan\Models\CadastroNewsletter;
use Suavipan\Models\Descadastro;

class DescadastrosController extends Controller
{
    public function index()
    {
        return view('site.descadastros.index');
    }

    public function postDescadastro(Request $request)
    {
        $cadastro = CadastroNewsletter::where('email', $request->email)->first();
        $cadastro->delete();

        $data = $request->all();
        Descadastro::create($data);

        return redirect()->route('descadastros.confirmacao');
    }

    public function indexConfirmacao()
    {
        return view('site.descadastros.confirmacao');
    }
}
