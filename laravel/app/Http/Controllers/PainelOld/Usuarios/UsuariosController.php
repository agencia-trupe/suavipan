<?php

namespace Suavipan\Http\Controllers\Painel\Usuarios;

use Illuminate\Http\Request;
use Suavipan\Http\Controllers\Controller;
use View;
use Hash;

use Suavipan\Models\User;

class UsuariosController extends Controller{

	public function index()
	{
		$usuarios = User::all();

		return view('painel.usuarios.index')->with(compact('usuarios'));
	}

	public function create()
	{
		return view('painel.usuarios.create');
	}

	public function store(Request $request)
	{
		$this->validate($request, [
    	'login'             => 'required|unique:users,login',
    	'password'         => 'required|min:6',
    	'password_confirm' => 'required|min:6|same:password',
  	]);

		$object = new User;

		$object->login     = $request->login;
		$object->email    = $request->email;
		$object->password = Hash::make($request->password);

		try {

			$object->save();

			$request->session()->flash('sucesso', 'Usuário criado com sucesso.');

			return redirect()->route('painel.usuarios.index');

		} catch (\Exception $e) {

			$request->flash();

			return back()->withErrors(array('Erro ao criar usuário! ('.$e->getMessage().')'));

		}
	}

	public function edit($id)
	{
		$usuario = User::find($id);

		return view('painel.usuarios.edit')->with(compact('usuario'));
	}

	public function update(Request $request, $id)
	{
		$this->validate($request, [
    	'login'             => 'required|unique:users,login,'.$id,
    	'password'         => 'min:6',
    	'password_confirm' => 'required_with:password|min:6|same:password',
  	]);

		$object = User::find($id);

		$object->email = $request->email;
		$object->login = $request->login;

		if($request->has('password'))
			$object->password = Hash::make($request->password);

		try {

			$object->save();

			$request->session()->flash('sucesso', 'Usuário alterado com sucesso.');

			return redirect()->route('painel.usuarios.index');

		} catch (\Exception $e) {

			$request->flash();

			return back()->withErrors(array('Erro ao criar usuário! ('.$e->getMessage().')'));

		}
	}

	public function destroy(Request $request, $id)
	{
		$object = User::find($id);
		$object->delete();

		$request->session()->flash('sucesso', 'Usuário removido com sucesso.');

		return redirect()->route('painel.usuarios.index');
	}
}
