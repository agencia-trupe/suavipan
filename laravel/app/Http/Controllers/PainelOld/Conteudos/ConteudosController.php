<?php

namespace Suavipan\Http\Controllers\Painel\Conteudos;

use Illuminate\Http\Request;
use Suavipan\Http\Controllers\Controller;
use View;
use Hash;
use DB;

use Suavipan\Libs\Thumbs;
use Suavipan\Models\Conteudo;

class ConteudosController extends Controller{

	public function getIndex()
	{
		return view('painel.conteudos.index')->with(['registros' => Conteudo::all()]);
	}

	public function postUpdate(Request $request)
	{
		$conteudos = $request->conteudos;
    foreach($conteudos as $indice => $valores){
      foreach($valores as $campo => $valor){
        $conteudo = Conteudo::indice($indice)->first();
        $conteudo->{$campo} = $valor;
        $conteudo->save();
      }
    }

		$arquivos = $request->arquivos;

		foreach($arquivos as $indice => $imagem){
			if($imagem){
				$filename = $imagem->getClientOriginalName();
				$conteudo = Conteudo::indice($indice)->first();
				$imagem->move('assets/img/conteudos', $filename);
				$conteudo->valor_pt_br = $filename;
				$conteudo->save();
			}
    }

		try {

			$request->session()->flash('sucesso', 'Conteúdo alterado com sucesso.');

			return redirect()->route('painel.conteudos.index');

		} catch (\Exception $e) {

			$request->flash();

			return back()->withErrors(array('Erro ao alterar conteúdo! ('.$e->getMessage().')'));

		}
	}

}
