<?php

namespace Suavipan\Http\Controllers\Painel\Produtos;

use Illuminate\Http\Request;
use Suavipan\Http\Controllers\Controller;

use Suavipan\Libs\Thumbs;
use Suavipan\Models\Tipo;
use Suavipan\Models\Linha;
use Suavipan\Models\Produto;


class ProdutosController extends Controller{

	public function index(Request $request)
	{
    $linha = Linha::findOrFail($request->linha_id);
    $tipo = Tipo::findOrFail($request->tipo_id);
		return view('painel.produtos.index')->with(compact('linha', 'tipo'));
	}
	
	public function create()
	{
		return view('painel.produtos.create');
	}

	public function edit($id)
	{
		$registro = Produto::find($id);
		return view('painel.produtos.edit')->with(compact('registro'));
	}

	public function update(Request $request, $id)
	{
		$this->validate($request, [
			'titulo_pt'       => 'required'
  	]);

		$object = Produto::find($id);

		$object->titulo_pt = $request->titulo_pt;
		$object->titulo_en = $request->titulo_en;
		$object->titulo_es = $request->titulo_es;
		$object->ingredientes_pt = $request->ingredientes_pt;
		$object->ingredientes_en = $request->ingredientes_en;
		$object->ingredientes_es = $request->ingredientes_es;
		$object->tabela_porcao_pt = $request->tabela_porcao_pt;
		$object->tabela_porcao_en = $request->tabela_porcao_en;
		$object->tabela_porcao_es = $request->tabela_porcao_es;
		$object->nutri_valor_calorico = $request->nutri_valor_calorico;
		$object->nutri_valor_calorico_pct = $request->nutri_valor_calorico_pct;
		$object->nutri_carboidratos = $request->nutri_carboidratos;
		$object->nutri_carboidratos_pct = $request->nutri_carboidratos_pct;
		$object->nutri_proteinas = $request->nutri_proteinas;
		$object->nutri_proteinas_pct = $request->nutri_proteinas_pct;
		$object->nutri_gorduras_totais = $request->nutri_gorduras_totais;
		$object->nutri_gorduras_totais_pct = $request->nutri_gorduras_totais_pct;
		$object->nutri_gorduras_saturadas = $request->nutri_gorduras_saturadas;
		$object->nutri_gorduras_saturadas_pct = $request->nutri_gorduras_saturadas_pct;
		$object->nutri_gorduras_trans = $request->nutri_gorduras_trans;
		$object->nutri_gorduras_trans_pct = $request->nutri_gorduras_trans_pct;
		$object->nutri_fibra_alimentar = $request->nutri_fibra_alimentar;
		$object->nutri_fibra_alimentar_pct = $request->nutri_fibra_alimentar_pct;
		$object->nutri_sodio = $request->nutri_sodio;
		$object->nutri_sodio_pct = $request->nutri_sodio_pct;

		$imagem = Thumbs::make($request, 'imagem', null, null, 'produtos/');
		if($imagem) $object->imagem = $imagem;

		try {

			$object->save();

			$request->session()->flash('sucesso', 'Produto alterado com sucesso.');

			return redirect()->route('painel.produtos.index', ['linha_id' => $object->linhas_id, 'tipo_id' => $object->tipos_id]);

		} catch (\Exception $e) {

			$request->flash();

			return back()->withErrors(array('Erro ao alterar Produto! ('.$e->getMessage().')'));

		}
	}

	public function destroy(Request $request, $id)
	{
		$object = Produto::find($id);

		$linha_id = $object->linhas_id;
		$tipo_id = $object->tipos_id;

		$object->delete();

		$request->session()->flash('sucesso', 'Produto removido com sucesso.');

		return redirect()->route('painel.produtos.index', ['linha_id' => $linha_id, 'tipo_id' => $tipo_id]);
	}

}
