<?php

namespace Suavipan\Http\Controllers\Painel\Linhas;

use Illuminate\Http\Request;
use Suavipan\Http\Controllers\Controller;
use View;
use Suavipan\Libs\Thumbs;
use Suavipan\Models\Linha;

class LinhasController extends Controller{

	public function index()
	{
    $registros = Linha::ordenacaoNormal()->get();
		return view('painel.linhas.index')->with(compact('registros'));
	}
	
	public function create()
	{
	  //dd("texto");
		return view('painel.linhas.create');
	}
	
	public function store(Request $request)
	{
		$this->validate($request, [
			'titulo_pt'       => 'required',
			'imagem_ativo'    => 'sometimes|image',
			'imagem_inativo'  => 'sometimes|image',
			'imagem_ampliada' => 'sometimes|image'
  	]);

		$object = new Linha;

		$object->titulo_pt = $request->titulo_pt;
		$object->titulo_en = $request->titulo_en;
		$object->titulo_es = $request->titulo_es;

		$object->slug_pt = str_slug($request->titulo_pt);
		$object->slug_en = str_slug($request->titulo_en);
		$object->slug_es = str_slug($request->titulo_es);

		$object->texto_pt = $request->texto_pt;
		$object->texto_en = $request->texto_en;
		$object->texto_es = $request->texto_es;

		$object->composicao_pt = $request->composicao_pt;
		$object->composicao_en = $request->composicao_en;
		$object->composicao_es = $request->composicao_es;

		$imagem_todosprods = Thumbs::make($request, 'imagem_todosprods', null, null, 'linhas/');
		if($imagem_todosprods) $object->imagem_todosprods = $imagem_todosprods;

		$imagem_todaslinhas = Thumbs::make($request, 'imagem_todaslinhas', null, null, 'linhas/');
		if($imagem_todaslinhas) $object->imagem_todaslinhas = $imagem_todaslinhas;

		$imagem_submenu = Thumbs::make($request, 'imagem_submenu', null, null, 'linhas/');
		if($imagem_submenu) $object->imagem_submenu = $imagem_submenu;

		$imagem_home_on = Thumbs::make($request, 'imagem_home_on', null, null, 'linhas/');
		if($imagem_home_on) $object->imagem_home_on = $imagem_home_on;

		$imagem_home_off = Thumbs::make($request, 'imagem_home_off', null, null, 'linhas/');
		if($imagem_home_off) $object->imagem_home_off = $imagem_home_off;

		$imagem_capa_linha = Thumbs::make($request, 'imagem_capa_linha', null, null, 'linhas/');
		if($imagem_capa_linha) $object->imagem_capa_linha = $imagem_capa_linha;


		try {

			$object->save();

			$request->session()->flash('sucesso', 'Linha incluida com sucesso.');

			return redirect()->route('painel.linhas.index');

		} catch (\Exception $e) {

			$request->flash();

			return back()->withErrors(array('Erro ao alterar Linha! ('.$e->getMessage().')'));

		}
	}

	

	public function edit($id)
	{
		$registro = Linha::find($id);
		return view('painel.linhas.edit')->with(compact('registro'));
	}

	public function update(Request $request, $id)
	{
		$this->validate($request, [
			'titulo_pt'       => 'required',
			'imagem_ativo'    => 'sometimes|image',
			'imagem_inativo'  => 'sometimes|image',
			'imagem_ampliada' => 'sometimes|image'
  	]);

		$object = Linha::find($id);

		$object->titulo_pt = $request->titulo_pt;
		$object->titulo_en = $request->titulo_en;
		$object->titulo_es = $request->titulo_es;

		$object->slug_pt = str_slug($request->titulo_pt);
		$object->slug_en = str_slug($request->titulo_en);
		$object->slug_es = str_slug($request->titulo_es);

		$object->texto_pt = $request->texto_pt;
		$object->texto_en = $request->texto_en;
		$object->texto_es = $request->texto_es;

		$object->composicao_pt = $request->composicao_pt;
		$object->composicao_en = $request->composicao_en;
		$object->composicao_es = $request->composicao_es;

		$imagem_todosprods = Thumbs::make($request, 'imagem_todosprods', null, null, 'linhas/');
		if($imagem_todosprods) $object->imagem_todosprods = $imagem_todosprods;

		$imagem_todaslinhas = Thumbs::make($request, 'imagem_todaslinhas', null, null, 'linhas/');
		if($imagem_todaslinhas) $object->imagem_todaslinhas = $imagem_todaslinhas;

		$imagem_submenu = Thumbs::make($request, 'imagem_submenu', null, null, 'linhas/');
		if($imagem_submenu) $object->imagem_submenu = $imagem_submenu;

		$imagem_home_on = Thumbs::make($request, 'imagem_home_on', null, null, 'linhas/');
		if($imagem_home_on) $object->imagem_home_on = $imagem_home_on;

		$imagem_home_off = Thumbs::make($request, 'imagem_home_off', null, null, 'linhas/');
		if($imagem_home_off) $object->imagem_home_off = $imagem_home_off;

		$imagem_capa_linha = Thumbs::make($request, 'imagem_capa_linha', null, null, 'linhas/');
		if($imagem_capa_linha) $object->imagem_capa_linha = $imagem_capa_linha;


		try {

			$object->save();

			$request->session()->flash('sucesso', 'Linha alterada com sucesso.');

			return redirect()->route('painel.linhas.index');

		} catch (\Exception $e) {

			$request->flash();

			return back()->withErrors(array('Erro ao incluir Linha! ('.$e->getMessage().')'));

		}
	}

}
