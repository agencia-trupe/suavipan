<?php

namespace Suavipan\Http\Controllers\Site\Contato;

use Illuminate\Http\Request;
use Suavipan\Http\Controllers\Controller;
use Mail;

class ContatoController extends Controller
{

  public function getIndex(Request $request)
  {
    return view('site.contato.index');
  }


  public function postEnviar(Request $request)
  {
    $this->validate($request, [
      'nome' => 'required',
      'email' => 'required|email',
      'mensagem' => 'required'
    ]);

    $data['nome'] = $request->input('nome');
    $data['email'] = $request->input('email');
    $data['telefone'] = $request->input('telefone');
    $data['mensagem'] = $request->input('mensagem');

    Mail::send('emails.contato', $data, function($message) use ($data)
    {
      $message->to('contato@suavipan.com.br')
              ->subject('Contato via site - '.$data['nome'])
              ->bcc('bruno@trupe.net')
              ->replyTo($data['email'], $data['nome']);
    });


    $request->session()->flash('contato_enviado', true);

    return redirect()->route('contato');
  }
}
