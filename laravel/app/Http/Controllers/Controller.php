<?php

namespace Suavipan\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesResources;
use App;
use Suavipan\Models\Linha;

class Controller extends BaseController
{
  use AuthorizesRequests, AuthorizesResources, DispatchesJobs, ValidatesRequests;

  public function __construct(){
    App::setLocale(session('locale', 'pt-br'));
    view()->share('linha_integral', Linha::findBySlug('integral'));
    view()->share('linha_levslim', Linha::findBySlug('levslim'));
    view()->share('linha_organico', Linha::findBySlug('organico'));
    view()->share('linha_zero', Linha::findBySlug('zero'));
  }
}
