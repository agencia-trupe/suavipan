<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
Route::group([
  'namespace' => 'Site'
], function () {

  Route::get('/', ['as' => 'home', 'uses' => 'Home\HomeController@getIndex']);
  Route::post('cadastro-newsletter', ['as' => 'cadastro.newsletter', 'uses' => 'Home\HomeController@postCadastroNewsletter']);

  Route::get('idioma/{sigla_idioma?}', function (Request $request, $sigla_idioma) {
    if ($sigla_idioma == 'pt-br' || $sigla_idioma == 'en' || $sigla_idioma == 'es') {
      Session::put('locale', $sigla_idioma);
      App::setLocale($sigla_idioma);
    }

    return Redirect::back();
  });

  //Rotas temporárias -- RETIRAR DEPOIS DO JOB--
  // Route::get('temp/home', ['as' => 'home', 'uses' => 'Home\HomeController@getIndexTemp']);
  Route::get('temp/nossas-linhas', ['as' => 'nossas-linhas', 'uses' => 'NossasLinhas\NossasLinhasController@getIndexTemp']);
  Route::get('temp/nossas-linhas/{slug_linha}', ['as' => 'nossas-linhas.detalhes', 'uses' => 'NossasLinhas\NossasLinhasController@getIndexTemp']);
  Route::get('temp/nossas-linhas/{slug_linha}/{slug_produto}', ['as' => 'nossas-linhas.detalhes-produto', 'uses' => 'NossasLinhas\NossasLinhasController@getIndexTemp']);
  //Fim da rotas temporárias---------------------  

  Route::get('todos-produtos', ['as' => 'todos-produtos', 'uses' => 'TodosProdutos\TodosProdutosController@getIndex']);
  Route::post('todos-produtos', ['as' => 'busca', 'uses' => 'TodosProdutos\TodosProdutosController@postBusca']);
  Route::get('nossas-linhas', ['as' => 'nossas-linhas', 'uses' => 'NossasLinhas\NossasLinhasController@getIndex']);
  Route::get('nossas-linhas/{slug_linha}', ['as' => 'nossas-linhas.detalhes', 'uses' => 'NossasLinhas\NossasLinhasController@getIndex']);
  Route::get('nossas-linhas/{slug_linha}/{slug_produto}', ['as' => 'nossas-linhas.detalhes-produto', 'uses' => 'NossasLinhas\NossasLinhasController@getIndex']);
  Route::get('a-suavipan', ['as' => 'a-suavipan', 'uses' => 'ASuavipan\ASuavipanController@getIndex']);
  Route::get('contato', ['as' => 'contato', 'uses' => 'Contato\ContatoController@getIndex']);
  Route::post('contato', ['as' => 'contato.enviar', 'uses' => 'Contato\ContatoController@postEnviar']);

  Route::get('politica-de-privacidade', 'PoliticaDePrivacidadeController@index')->name('politica-de-privacidade');
  Route::post('aceite-de-cookies', 'Home\HomeController@postCookies')->name('aceite-de-cookies.post');

  Route::get('descadastro', 'Contato\DescadastrosController@index')->name('descadastros');
  Route::post('descadastro', 'Contato\DescadastrosController@postDescadastro')->name('descadastros.post');
  Route::get('descadastro/confirmacao', 'Contato\DescadastrosController@indexConfirmacao')->name('descadastros.confirmacao');
});

// Authentication routes
Route::get('painel/auth/login', ['as' => 'painel.login', 'uses' => 'Painel\Auth\AuthController@getLogin']);
Route::post('painel/auth/login', ['as' => 'painel.auth', 'uses' => 'Painel\Auth\AuthController@postLogin']);
Route::get('painel/auth/logout', ['as' => 'painel.logout', 'uses' => 'Painel\Auth\AuthController@getLogout']);

Route::group([
  'middleware' => 'auth',
  'namespace' => 'Painel',
  'prefix' => 'painel'
], function () {

  Route::get('/', ['as' => 'painel.dashboard', 'uses' => function () {
    return view('painel.dashboard.index');
  }]);

  Route::post('gravar-ordem-registros', function (Illuminate\Http\Request $request) {
    $itens = $request->input('data');
    $tabela = $request->input('tabela');
    for ($i = 0; $i < count($itens); $i++)
      DB::table($tabela)->where('id', $itens[$i])->update(array('ordem' => $i));
  });


  Route::resource('usuarios', 'Usuarios\UsuariosController');
  Route::resource('linhas', 'Linhas\LinhasController');

  Route::resource('politica-de-privacidade', 'PoliticaDePrivacidadeController', ['only' => ['index', 'update']]);
  Route::get('aceite-de-cookies', 'AceiteDeCookiesController@index')->name('painel.aceite-de-cookies');
  // Route::get('painel/linhas', ['as' => 'painel.linhas','uses' => 'Painel\Linhas\LinhasController@getIndex']);

  //  Route::get('linhas', ['as' => 'painel.linhas.index', 'uses' => 'Linhas\LinhasController@getIndex']);

  // CONTEÚDOS SEPARADOS
  // Route::get('conteudos', ['as' => 'painel.conteudos.index', 'uses' => 'Conteudos\ConteudosController@getIndex']);
  // Route::post('conteudos', ['as' => 'painel.conteudos.update', 'uses' => 'Conteudos\ConteudosController@postUpdate']);
  Route::get('conteudos/home', ['as' => 'painel.home.index', 'uses' => 'Conteudos\ConteudosController@indexHome']);
  Route::post('conteudos/home', ['as' => 'painel.home.update', 'uses' => 'Conteudos\ConteudosController@updateHome']);
  Route::get('conteudos/todos-os-produtos', ['as' => 'painel.todos-os-produtos.index', 'uses' => 'Conteudos\ConteudosController@indexProdutos']);
  Route::post('conteudos/todos-os-produtos', ['as' => 'painel.todos-os-produtos.update', 'uses' => 'Conteudos\ConteudosController@updateProdutos']);
  Route::get('conteudos/nossas-linhas', ['as' => 'painel.nossas-linhas.index', 'uses' => 'Conteudos\ConteudosController@indexLinhas']);
  Route::post('conteudos/nossas-linhas', ['as' => 'painel.nossas-linhas.update', 'uses' => 'Conteudos\ConteudosController@updateLinhas']);
  Route::get('conteudos/a-suavipan', ['as' => 'painel.a-suavipan.index', 'uses' => 'Conteudos\ConteudosController@indexSuavipan']);
  Route::post('conteudos/a-suavipan', ['as' => 'painel.a-suavipan.update', 'uses' => 'Conteudos\ConteudosController@updateSuavipan']);
  Route::get('conteudos/contato', ['as' => 'painel.contato.index', 'uses' => 'Conteudos\ConteudosController@indexContato']);
  Route::post('conteudos/contato', ['as' => 'painel.contato.update', 'uses' => 'Conteudos\ConteudosController@updateContato']);

  //Route::resource('linhas', 'Linhas\LinhasController', ['only' => ['index', 'create', 'edit', 'update']]);
  Route::resource('tipos', 'Tipos\TiposController', ['only' => ['index', 'create', 'store', 'edit', 'update', 'destroy']]);
  Route::resource('produtos', 'Produtos\ProdutosController', ['only' => ['index', 'create', 'store', 'edit', 'update', 'destroy']]);
  
  Route::get('cadastros/download', ['as' => 'painel.cadastros.download', 'uses' => 'Cadastros\CadastrosController@getDownload']);
  Route::resource('cadastros', 'Cadastros\CadastrosController', ['only' => ['index', 'destroy']]);

  Route::get('descadastros/download', 'Cadastros\DescadastrosController@getDownload')->name('painel.descadastros.download');
  Route::resource('descadastros', 'Cadastros\DescadastrosController', ['only' => ['index', 'destroy']]);

  // NOVAS ROTAS DO PAINEL:

});
