<?php

namespace Suavipan\Http\Requests;

use Suavipan\Http\Requests\Request;

class PoliticaDePrivacidadeRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'texto' => 'required',
        ];
    }
}
