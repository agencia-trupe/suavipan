<?php

/**
 * Busca o valor de um termo na base de dados de acordo com a linguagem
 * selecionada
 *
 * @param  string  $indice
 * @return string
 */
if ( ! function_exists('conteudo'))
{
	function conteudo($indice)
	{
		$conteudo = Suavipan\Models\Conteudo::indice($indice)->first();

		if(!$conteudo) return 'conteúdo não encontrado';
		//if(!$conteudo) dd('conteúdo não encontrado');

		if($conteudo->tipo == 'imagem') return $conteudo->valor_pt_br;

		$campo = 'valor_';
		$campo .= App::getLocale() == 'pt-br' ? 'pt_br' : App::getLocale();

		return $conteudo->{$campo};
	}
}

/**
 * Return the base URL
 *
 * @return string
 */
if ( ! function_exists('base_url'))
{
	function base_url()
	{
		//return 'http://'.$_SERVER['HTTP_HOST'].'/p4revia-suavipan/';
		return 'http://'.$_SERVER['HTTP_HOST'];
	}
}


/**
 * Limit the number of words in a string.
 *
 * @param  string  $value
 * @param  int     $words
 * @param  string  $end
 * @return string
 */
if ( ! function_exists('str_words'))
{
	function str_words($value, $words = 100, $end = '...')
	{
		preg_match('/^\s*+(?:\S++\s*+){1,'.$words.'}/u', $value, $matches);

		if ( ! isset($matches[0])) return $value;

		if (strlen($value) == strlen($matches[0])) return $value;

		return rtrim($matches[0]).$end;
	}
}

/**
 *
 * Simply adds the http:// part if no scheme is included
 *
 * @access	public
 * @param	string	$str
 * @return	string
 */
if ( ! function_exists('prep_url'))
{
	function prep_url($str = '')
	{
		if ($str == 'https://' OR $str == 'http://' OR $str == '')
		{
			return '';
		}

		$url = parse_url($str);

		if ( ! $url OR ! isset($url['scheme']))
		{
			$str = 'http://'.$str;
		}

		return $str;
	}
}

/**
 *
 * Generate embed code to a given 'google maps incorporation code'
 *
 * @access	public
 * @param	string	$str
 * @param	string  $width
 * @param	string  $height
 * @param	string  $classe
 * @return	string
 */
if ( ! function_exists('embed_maps'))
{
	function embed_maps($str = '', $width = '', $height = '', $classe = '')
	{

	    //$str = stripslashes(htmlspecialchars_decode($str));

	    if($width != '')
	        $str = preg_replace("~width=\"(\d+)\"~", 'width="'.$width.'"', $str);
	    else
	    	$str = preg_replace("~width=\"(\d+)\"~", '', $str);

	    if($height != '')
	        $str = preg_replace("~height=\"(\d+)\"~", 'height="'.$height.'"', $str);
	    else
	    	$str = preg_replace("~height=\"(\d+)\"~", '', $str);

	    if($classe != '')
	    	$str = preg_replace("~<iframe~", "<iframe class='{$classe}'", $str);

		// COM o link 'ver mapa ampliado'
	    // return $str;

	    // SEM o link 'ver mapa ampliado'
	    return preg_replace('~<br \/>(.*)~', '', $str);
	}
}

/**
 *
 * Separate the username from a Instagram URL
 *
 * @access	public
 * @param	string	$url
 * @return	string
 */
if ( ! function_exists('get_social_user'))
{
	function get_social_user($url = '', $prefixo = '@', $offset = 0)
	{
	    $username = '';

	    $parse = parse_url($url);

    	if($parse && isset($parse['host'])){

	    	if(strpos($parse['host'], 'tumblr') !== false){

	    		$xpd = explode('.', $parse['host']);
		    	$retorno = isset($xpd[0 + $offset]) ? $xpd[0 + $offset] : '';

	    	}elseif(isset($parse['path']) && $parse['path'] != '/'){

		    	$xpd = explode('/', $parse['path']);
		    	$retorno = isset($xpd[1 + $offset]) ? $xpd[1 + $offset] : '';

	    	}

	    	return $prefixo.$retorno;

	    }else{
	    	return '';
	    }
	}
}

/**
 *
 * Return youtube embed url based on a Youtube video id
 *
 * @access	public
 * @param		string	$url
 * @return	string
 */
if ( ! function_exists('youtube_embed_link'))
{
	function youtube_embed_link($video_id)
	{
	  return "<iframe src='http://www.youtube.com/embed/{$video_id}?autoplay=0' frameborder='0' allowfullscreen></iframe>";
	}
}
