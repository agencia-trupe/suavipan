<?php

return [
  'menu.pagina-inicial' => 'Página Inicial',

  'linha_integral' => 'Linha Integral',
  'linha_levslim' => 'Linha LevSlim',
  'linha_organico' => 'Linha Orgânico',
  'linha_zero' => 'Linha Zero',

  'menu.todos-produtos' => 'Todos os Produtos',
  'menu.produtos' => 'Produtos',
  'menu.nossas-linhas' => 'Nossas Linhas',
  'menu.a-suavipan' => 'A SuaviPan',
  'menu.contato' => 'Contato',
  'menu.versao-pt-br' => 'versão em português',
  'menu.versao-es' => 'versão em espanhol',
  'menu.versao-en' => 'versão em inglês',
  'menu.entre-em-contato' => 'Entre em contato',
  'menu.direitos' => 'Todos os direitos reservados',

  'form-newsletter.titulo' => 'CADASTRE-SE PARA RECEBER NOVIDADES!',
  'form.nome' => 'nome',
  'form.email' => 'email',
  'form.enviar' => 'ENVIAR',
  'form-newsletter.resposta' => 'E-mail cadastrado com sucesso!',
  'compartilhe.titulo' => 'ACOMPANHE E COMPARTILHE!',

  'busca.placeholder' => 'buscar produto',
  'busca.nenhum_resultado' => 'Nenhum resultado encontrado',
  'busca.um_resultado' => '1 resultado encontrado',
  'busca.resultados_encontrados' => 'resultados encontrados',

  'produtos.ver-detalhes' => 'Ver detalhes do produto',

  'detalhes.ingredientes' => 'INGREDIENTES',
  'detalhes.voltar' => 'Ver outros produtos da linha ',
  'tabela_nutri.titulo' => 'INFORMAÇÃO NUTRICIONAL',
  'tabela_nutri.quantidade' => 'QUANTIDADE POR PORÇÃO',
  'tabela_nutri.vd' => '%VD (*)',
  'tabela_nutri.valor_calorico' => 'Valor calórico',
  'tabela_nutri.carboidratos' => 'Carboidratos',
  'tabela_nutri.proteinas' => 'Proteínas',
  'tabela_nutri.gorduras_totais' => 'Gorduras totais',
  'tabela_nutri.gorduras_saturadas' => 'Gorduras saturadas',
  'tabela_nutri.gorduras_trans' => 'Gorduras trans',
  'tabela_nutri.fibra_alimentar' => 'Fibra alimentar',
  'tabela_nutri.sodio' => 'Sódio',
  'tabela_nutri.obs' => '* Valores diários de referência com base em uma dieta de 2.000 Kcal ou 8.400 kJ.',

  'quemsomos.visao' => 'VISÃO',
  'quemsomos.missao' => 'MISSÃO',
  'quemsomos.valores' => 'VALORES',
  'quemsomos.associacao' => 'Associado a:',

  'contato.nome' => 'nome',
  'contato.email' => 'e-mail',
  'contato.telefone' => 'telefone',
  'contato.mensagem' => 'mensagem',
  'contato.enviar' => 'ENVIAR'
];
