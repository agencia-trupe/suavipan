@extends('site.template')

@section('conteudo')

<div class="conteudo conteudo-contato">
  <div class="centralizar">
    <div class="coluna reduzida"></div>
    <div class="coluna">
      <h1>{{conteudo('contato.titulo')}}</h1>

      <h2>11 2096-7069</h2>

      <h2>{{conteudo('contato.telefone')}}</h2>

      <div class="resposta resposta-form">

        @if( $errors->has('nome') ||
        $errors->has('email') ||
        $errors->has('mensagem'))
        <div class="erro">{{ $errors->first() }}</div>
        @endif

        @if(session('contato_enviado'))
        <div class="sucesso">Sua mensagem foi enviada com Sucesso!</div>
        @endif

      </div>

      <form action="{{URL::route('contato')}}" method="post" novalidate >
        {!!csrf_field()!!}
        <input type="text" name="nome" required placeholder="{{trans('site.contato.nome')}}" value="{{old('nome')}}">
        <input type="email" name="email" required placeholder="{{trans('site.contato.email')}}" value="{{old('email')}}">
        <input type="text" name="telefone" placeholder="{{trans('site.contato.telefone')}}" value="{{old('telefone')}}">
        <textarea name="mensagem" required placeholder="{{trans('site.contato.mensagem')}}">{{old('mensagem')}}</textarea>

        <div class="contato-opt-in">
          <input type="checkbox" id="optInContato" name="optIn" value="1" required>
          <p class="texto-lgpd">Aceito que ao fornecer meus dados para contato a Suavipan os utilizará apenas para responder minha solicitação ou fazer marketing da própria empresa (divulgação de conteúdo, serviços ou produtos relevantes). Meus dados não serão compartilhados ou utilizados para outros fins. Estou ciente de que poderei me excluir do mailing da empresa a qualquer momento através do link localizado nos e-mails enviados, nos termos da Lei 13.709/18 – LGPD.</p>
        </div>

        <input type="submit" class="btn-enviar-contato" value="ENVIAR">
      </form>
    </div>
  </div>
</div>

@stop