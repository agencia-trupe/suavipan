@extends('site.template')

@section('conteudo')

    <div class="conteudo politica-de-privacidade">
        <h2 class="titulo">POLÍTICA DE PRIVACIDADE</h2>
        <div class="texto">
            {!! $politica->texto !!}
        </div>
    </div>

@stop