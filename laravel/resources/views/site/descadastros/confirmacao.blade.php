@extends('site.template')

@section('conteudo')

<div class="conteudo conteudo-descadastro-confirmacao">
  <div class="centralizar">
    <h1>DESCADASTRAR</h1>
    <p class="frase1">Seu e-mail será retirado de nossa listagem em até 48 horas.</p>
    <p class="frase2">Nosso website e outros canais de contato continuam à sua disposição.</p>
    <p class="frase3">Gratos.</p>
  </div>
</div>

@stop