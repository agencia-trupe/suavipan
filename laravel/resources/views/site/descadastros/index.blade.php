@extends('site.template')

@section('conteudo')

<div class="conteudo conteudo-descadastro">
  <div class="centralizar">
    <h1>DESCADASTRAR</h1>
    <p class="frase">Para solicitar a retirada do seu contato de nossa lista de contatos informe seu e-mail:</p>

    <form action="{{URL::route('descadastros.post')}}" method="post" novalidate>
      {!!csrf_field()!!}
      <input type="email" name="email" required placeholder="{{trans('site.contato.email')}}" value="{{old('email')}}">
      <input type="submit" value="ENVIAR">
    </form>
  </div>
</div>

@stop