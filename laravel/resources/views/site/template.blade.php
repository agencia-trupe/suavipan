<!DOCTYPE html>
<html lang="pt-BR" class="no-js">

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="robots" content="index, follow" />
	<meta name="author" content="Trupe Design" />
	<meta name="copyright" content="2016 Trupe Design" />
	<meta name="viewport" content="width=device-width,initial-scale=1">

	<meta name="keywords" content="bolinho, bolo, zero, orgânico, integral, suavipan, alfajor, light, alimento, produto, barrinha, bolo de caneca, levslim, grainfit" />

	<title>SuaviPan | Produtos Zero, Integrais e Orgânicos</title>
	<meta property="og:title" content="SuaviPan | Produtos Zero, Integrais e Orgânicos" />

	<meta name="description" content="A SuaviPan possui uma linha completa de produtos saudáveis e saborosos que são uma ótima opção para todos os momentos do seu dia.">
	<meta property="og:description" content="A SuaviPan possui uma linha completa de produtos saudáveis e saborosos que são uma ótima opção para todos os momentos do seu dia." />

	<meta property="og:site_name" content="SuaviPan | Produtos Zero, Integrais e Orgânicos" />
	<meta property="og:type" content="website" />
	<meta property="og:url" content="{{ Request::url() }}" />

	<meta property="og:image" content="{{ base_url() }}/assets//img/layout/imgFB-suavipan.png" />
	<meta property="og:image:width" content="960" />
	<meta property="og:image:height" content="630" />


	<link rel="apple-touch-icon" sizes="57x57" href="/apple-icon-57x57.png">
	<link rel="apple-touch-icon" sizes="60x60" href="/apple-icon-60x60.png">
	<link rel="apple-touch-icon" sizes="72x72" href="/apple-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="76x76" href="/apple-icon-76x76.png">
	<link rel="apple-touch-icon" sizes="114x114" href="/apple-icon-114x114.png">
	<link rel="apple-touch-icon" sizes="120x120" href="/apple-icon-120x120.png">
	<link rel="apple-touch-icon" sizes="144x144" href="/apple-icon-144x144.png">
	<link rel="apple-touch-icon" sizes="152x152" href="/apple-icon-152x152.png">
	<link rel="apple-touch-icon" sizes="180x180" href="/apple-icon-180x180.png">
	<link rel="icon" type="image/png" sizes="192x192" href="/android-icon-192x192.png">
	<link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="96x96" href="/favicon-96x96.png">
	<link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
	<link rel="manifest" href="/manifest.json">
	<meta name="msapplication-TileColor" content="#ffffff">
	<meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
	<meta name="theme-color" content="#ffffff">

	<base href="{{ base_url() }}">
	<script>
		var BASE = "{{ base_url() }}"
	</script>

	<link href='https://fonts.googleapis.com/css?family=Amatic+SC:400,700|Open+Sans:400,700,400italic,700italic' rel='stylesheet' type='text/css'>

	<link rel="stylesheet" href="assets/css/vendor.css">

	<link rel="stylesheet" href="assets/css/site.css">
	<link rel="stylesheet" href="{{ asset('assets/css/complemento.css') }}">

	<script src="assets/js/jquery.js"></script>
	<!-- <script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.0/jquery.min.js"></script> -->

	<style>
		#centro {
			width: 300px;
			height: 420px;
			position: absolute;
			top: 40%;
			left: 50%;
			margin-top: -150px;
			margin-left: -150px;
			z-index: 999;
		}

		.close {
			position: absolute;
			right: 5px;
			top: 5px;
			cursor: pointer;
			color: #fff;

		}

		#backg {
			position: absolute;
			width: 100%;
			height: 100%;
			opacity: 0.8;
			background-color: #000;
			z-index: 900;

		}

		.vinte {
			display: inline-block;
			*display: inline;
			zoom: 1;
			vertical-align: top;
			width: 45%;
			max-width: 370px;
			margin-right: 10px;
		}
	</style>

</head>

<body>

	@include('site.partials.menu')

	@yield('conteudo')

	@include('site.partials.compartilhe')

	@include('site.partials.footer')

	<!-- AVISO DE COOKIES -->
	@if(!isset($verificacao))
	<div class="aviso-cookies">
		<meta name="csrf-token" content="{{ csrf_token() }}" />
		<p class="frase-cookies">Usamos cookies para personalizar o conteúdo, acompanhar anúncios e oferecer uma experiência de navegação mais segura a você. Ao continuar navegando em nosso site você concorda com o uso dessas informações. Leia nossa <a href="{{ route('politica-de-privacidade') }}" target="_blank" class="link-politica">Política de Privacidade</a> e saiba mais.</p>
		<button class="aceitar-cookies">ACEITAR E FECHAR</button>
	</div>
	@endif

	<script src="assets/js/site.js"></script>

	<script type="text/javascript">
		var routeHome = '{{ route("home") }}' + "/";

		$(document).ready(function() {
			// AVISO DE COOKIES
			$(".aviso-cookies").hide();

			if (window.location.href == routeHome) {
				$(".aviso-cookies").show();

				$(".aceitar-cookies").click(function() {
					var url = window.location.origin + "/aceite-de-cookies";
					var CSRF_TOKEN = $('.aviso-cookies meta[name="csrf-token"]').attr(
						"content"
					);

					$.ajax({
						type: "POST",
						url: url,
						data: {
							_token: CSRF_TOKEN
						},
						success: function(data, textStatus, jqXHR) {
							$(".aviso-cookies").hide();
						},
						error: function(jqXHR, textStatus, errorThrown) {
							console.log(jqXHR, textStatus, errorThrown);
						},
					});
				});
			}

			// Checkbox aviso LGPD
			$(".btn-enviar-contato").click(function() {
				if ($('input[type="checkbox"]#optInContato').prop("checked") == true) {} else {
					alert("É necessário aceitar os termos.");
				}
			});
			$(".btn-enviar-newsletter").click(function() {
				if ($('input[type="checkbox"]#optInNewsletter').prop("checked") == true) {} else {
					alert("É necessário aceitar os termos.");
				}
			});
		});
	</script>

	<!-- Google Analytics -->
	<script>
		(function(i, s, o, g, r, a, m) {
			i['GoogleAnalyticsObject'] = r;
			i[r] = i[r] || function() {
				(i[r].q = i[r].q || []).push(arguments)
			}, i[r].l = 1 * new Date();
			a = s.createElement(o),
				m = s.getElementsByTagName(o)[0];
			a.async = 1;
			a.src = g;
			m.parentNode.insertBefore(a, m)
		})(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

		ga('create', 'UA-80432864-1', 'auto');
		ga('send', 'pageview');
	</script>

	<script type="text/javascript">
		function ocultar() {
			document.getElementById("centro").style.display = "none";
			document.getElementById("backg").style.display = "none";
		}
	</script>
	<!-- End Google Analytics -->
</body>

</html>