@extends('site.template')

@section('conteudo')

  <div class="conteudo conteudo-todos-produtos">

    <div class="centralizar">

      <form action="{{URL::route('busca')}}" method="post" id="form-busca">
        {!!csrf_field()!!}
        <input type="text" name="busca" placeholder="{{trans('site.busca.placeholder')}}">
        <input type="submit" value="Buscar">
      </form>

      <div class="topo-produtos">
        <img src="assets/img/conteudos/{{conteudo('produtos.imagem')}}" alt="{{conteudo('produtos.titulo')}}">
        <div class="chamada">{{conteudo('produtos.chamada')}}</div>
        <h1>{{conteudo('produtos.titulo')}}</h1>
        <h2>{{conteudo('produtos.subtitulo')}}</h2>
      </div>

    </div>

    <div class="lista-linhas">
      @foreach($listaLinhas as $linha)
        <div class="linha" style='background-color:{{$linha->cor}}'>
          <div class="centralizar">
            <div class="coluna-marca">
              <img src="assets/img/linhas/{{$linha->imagem_todosprods}}" alt="{{$linha->traduz('titulo')}}">
            </div>
            <div class="coluna-produtos">
              <div class="lista-produtos">
                @foreach($linha->tipos as $tipo)
                  @foreach($tipo->produtos as $produto)
                    <div class="produto">
                      <a href="nossas-linhas/{{$linha->slug_pt}}/{{$tipo->slug_pt}}#{{$produto->slug_pt}}" title="{{$produto->traduz('titulo')}}">
                        @if($produto->imagem)
                          <img src="assets/img/produtos/{{$produto->imagem}}" alt="{{$produto->traduz('titulo')}}">
                        @endif
                        <div class="tipo">{{$tipo->traduz('titulo')}}</div>
                        <div class="titulo">{{$produto->traduz('titulo')}}</div>
                      </a>
                    </div>
                  @endforeach
                @endforeach
              </div>
            </div>
          </div>
        </div>
      @endforeach
    </div>

  </div>

@stop
