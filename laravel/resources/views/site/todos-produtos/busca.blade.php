@extends('site.template')

@section('conteudo')

  <div class="conteudo conteudo-todos-produtos">

    <div class="centralizar">

      <form action="{{URL::route('busca')}}" method="post" id="form-busca">
        {!!csrf_field()!!}
        <input type="text" name="termo" placeholder="{{trans('site.busca.placeholder')}}" value="{{$termo or ''}}">
        <input type="submit" value="Buscar">
      </form>

      <div class="topo-produtos">
        <img src="assets/img/conteudos/{{conteudo('produtos.imagem')}}" alt="{{conteudo('produtos.titulo')}}">
        <div class="chamada">
          @if(sizeof($resultados) == 0)
            {{trans('site.busca.nenhum_resultado')}}
          @elseif(sizeof($resultados) == 1)
            {{trans('site.busca.um_resultado')}}
          @else
            {{sizeof($resultados)}} {{trans('site.busca.resultados_encontrados')}}
          @endif
        </div>
      </div>

      <div class="lista-resultados-busca">
        @foreach($resultados as $produto)
          <div class="produto" id="{{$produto->slug_pt}}">
            <div class="coluna esquerda">
              @if($produto->imagem)
                <img src="assets/img/produtos/{{$produto->imagem}}" alt="{{$produto->tipo->traduz('titulo').' '.$produto->traduz('titulo')}}" />
              @endif
            </div>
            <div class="coluna direita">
              <h1>{{$produto->tipo->traduz('titulo')}}</h1>
              <h2>{{$produto->traduz('titulo')}}</h2>
              <div class="info">
                <div class="texto">
                  <h3>{{trans('site.detalhes.ingredientes')}}</h3>
                  <div class="cke">
                    {!! $produto->traduz('ingredientes') !!}
                  </div>
                </div>
                <div class="tabela">
                  <table>
                    <thead>
                      <tr>
                        <th colspan="3">
                          {{trans('site.tabela_nutri.titulo')}}
                          <br>
                          {{$produto->traduz('tabela_porcao')}}
                        </th>
                      </tr>
                      <tr>
                        <th colspan="2">{{trans('site.tabela_nutri.quantidade')}}</th>
                        <th>{{trans('site.tabela_nutri.vd')}}</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td>{{trans('site.tabela_nutri.valor_calorico')}}</td>
                        <td>{{$produto->nutri_valor_calorico}}</td>
                        <td>{{$produto->nutri_valor_calorico_pct}}</td>
                      </tr>
                      <tr>
                        <td>{{trans('site.tabela_nutri.carboidratos')}}</td>
                        <td>{{$produto->nutri_carboidratos}}</td>
                        <td>{{$produto->nutri_carboidratos_pct}}</td>
                      </tr>
                      <tr>
                        <td>{{trans('site.tabela_nutri.proteinas')}}</td>
                        <td>{{$produto->nutri_proteinas}}</td>
                        <td>{{$produto->nutri_proteinas_pct}}</td>
                      </tr>
                      <tr>
                        <td>{{trans('site.tabela_nutri.gorduras_totais')}}</td>
                        <td>{{$produto->nutri_gorduras_totais}}</td>
                        <td>{{$produto->nutri_gorduras_totais_pct}}</td>
                      </tr>
                      <tr>
                        <td>{{trans('site.tabela_nutri.gorduras_saturadas')}}</td>
                        <td>{{$produto->nutri_gorduras_saturadas}}</td>
                        <td>{{$produto->nutri_gorduras_saturadas_pct}}</td>
                      </tr>
                      <tr>
                        <td>{{trans('site.tabela_nutri.gorduras_trans')}}</td>
                        <td>{{$produto->nutri_gorduras_trans}}</td>
                        <td>{{$produto->nutri_gorduras_trans_pct}}</td>
                      </tr>
                      <tr>
                        <td>{{trans('site.tabela_nutri.fibra_alimentar')}}</td>
                        <td>{{$produto->nutri_fibra_alimentar}}</td>
                        <td>{{$produto->nutri_fibra_alimentar_pct}}</td>
                      </tr>
                      <tr>
                        <td>{{trans('site.tabela_nutri.sodio')}}</td>
                        <td>{{$produto->nutri_sodio}}</td>
                        <td>{{$produto->nutri_sodio_pct}}</td>
                      </tr>
                    </tbody>
                    <tfoot>
                      <tr>
                        <td colspan="3">{{trans('site.tabela_nutri.obs')}}</td>
                      </tr>
                    </tfoot>
                  </table>
                </div>
              </div>
            </div>
          </div>
        @endforeach
      </div>

    </div>

  </div>

@stop
