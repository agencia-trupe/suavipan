@extends('site.template')

@section('conteudo')

  <div class="conteudo conteudo-nossas-linhas">

    <div id="submenu-linhas">
      <h1>Seleciona a Linha:</h1>
      <ul class="menu-linhas">
        @foreach($listaLinhas as $linha)
          <li>
            <a href="nossas-linhas/{{$linha->slug_pt}}" @if($linhaSelecionada && $linha->id == $linhaSelecionada->id) class="ativo" @endif title="{{$linha->traduz('titulo')}}" style="background-image:url('assets/img/linhas/{{$linha->imagem_submenu}}');">{{$linha->traduz('titulo')}}</a>
          </li>
        @endforeach
      </ul>
    </div>


    @if(!$linhaSelecionada && !$tipoSelecionado)

      <div class="centralizar topo-linhas">
        <div class="chamada">{{conteudo('linhas.chamada')}}</div>
        <h1>{{conteudo('linhas.titulo')}}</h1>
        <h2>{{conteudo('linhas.subtitulo')}}</h2>
        <img src="assets/img/conteudos/{{conteudo('linhas.imagem')}}" alt="{{conteudo('linhas.chamada')}}" />
      </div>

    @elseif($linhaSelecionada && !$tipoSelecionado)

      <div class="topo-detalhe-linha">
        <div class="centralizar">
          <div class="imagem">
            @if($linhaSelecionada->imagem_capa_linha)
              <img src="assets/img/linhas/{{$linhaSelecionada->imagem_capa_linha}}" alt="{{$linhaSelecionada->traduz('titulo')}}" />
            @endif
          </div>
          <div class="texto">
            <div class="descricao">{!! $linhaSelecionada->traduz('texto') !!}</div>
            <div class="composicao">{{$linhaSelecionada->traduz('composicao')}}</div>
            <div class="bolinhas">
              @if($linhaSelecionada->slug_pt == 'integral')
                <div class="bolinha integral-gorduras-trans"></div>
                <div class="bolinha integral-farinhas-integrais"></div>
                <div class="bolinha integral-zero-lactose"></div>
                <div class="bolinha integral-fibras"></div>
                <div class="bolinha integral-oleo-canola"></div>
                <div class="bolinha integral-acucar-mascavo"></div>
              @elseif($linhaSelecionada->slug_pt == 'levslim')
                <div class="bolinha levslim-gorduras-trans"></div>
                <div class="bolinha levslim-zero-acucar"></div>
                <div class="bolinha levslim-baixo-sodio"></div>
              @elseif($linhaSelecionada->slug_pt == 'organico')
                <div class="bolinha organico-nutricional"></div>
                <div class="bolinha organico-sem-agrotoxicos"></div>
                <div class="bolinha organico-meio-ambiente"></div>
                <div class="bolinha organico-mais-saude"></div>
                <div class="bolinha organico-ecocert"></div>
              @elseif($linhaSelecionada->slug_pt == 'zero')
                <div class="bolinha zero-zero-acucar"></div>
                <div class="bolinha zero-fibras"></div>
                <div class="bolinha zero-baixas-calorias"></div>
                <div class="bolinha zero-baixo-sodio"></div>
                <div class="bolinha zero-gorduras-trans"></div>
              @endif
            </div>
          </div>
        </div>
      </div>

      <div class="lista-tipos">
        @foreach($linhaSelecionada->tipos as $k => $tipo)
          <div class="tipo" style="background-color:{{$tipo->cor_fundo}};">
            <a href="nossas-linhas/{{$tipo->linha->slug_pt}}/{{$tipo->slug_pt}}" title="{{$tipo->traduz('titulo')}}">
              <div class="centralizar">
                @if($k%2 == 0)
                  <div class="imagem esq">
                    @if($tipo->imagem)
                      <img src="assets/img/tipos/{{$tipo->imagem}}" alt="">
                    @endif
                  </div>
                  <div class="detalhe">
                    <div class="titulo">{{$tipo->traduz('titulo')}}</div>
                    <div class="texto cke">{!! $tipo->traduz('chamada') !!}</div>
                    <div class="link">{{trans('site.produtos.ver-detalhes')}} &raquo;</div>
                  </div>
                @else
                  <div class="detalhe esq">
                    <div class="titulo">{{$tipo->traduz('titulo')}}</div>
                    <div class="texto cke">{!! $tipo->traduz('chamada') !!}</div>
                    <div class="link">{{trans('site.produtos.ver-detalhes')}} &raquo;</div>
                  </div>
                  <div class="imagem dir">
                    @if($tipo->imagem)
                      <img src="assets/img/tipos/{{$tipo->imagem}}" alt="">
                    @endif
                  </div>
                @endif
              </div>
            </a>
          </div>
        @endforeach
      </div>

      <div class="espacamento">
        <div class="centralizar"></div>
      </div>

    @elseif($linhaSelecionada && $tipoSelecionado)

      <div class="detalhes-produto">
        <div class="centralizar">

          <div class="topo">
            <div class="coluna esquerda">
              @if($linhaSelecionada->imagem_capa_linha)
                <img src="assets/img/linhas/{{$linhaSelecionada->imagem_capa_linha}}" alt="{{$linhaSelecionada->traduz('titulo')}}" />
              @endif
            </div>
            <div class="coluna direita imagem-superior">
              @if($tipoSelecionado->imagem)
                <img src="assets/img/tipos/{{$tipoSelecionado->imagem}}" alt="{{$linhaSelecionada->traduz('titulo')}}" />
              @endif
            </div>
          </div>

          <div class="lista-produtos">
            @foreach($tipoSelecionado->produtos as $produto)
              <div class="produto" id="{{$produto->slug_pt}}">
                <div class="coluna esquerda">
                  @if($produto->imagem)
                    <img src="assets/img/produtos/{{$produto->imagem}}" alt="{{$produto->tipo->traduz('titulo').' '.$produto->traduz('titulo')}}" />
                  @endif
                </div>
                <div class="coluna direita">
                  <h1>{{$produto->tipo->traduz('titulo')}}</h1>
                  <h2>{{$produto->traduz('titulo')}}</h2>
                  <div class="info">
                    <div class="texto">
                      <h3>{{trans('site.detalhes.ingredientes')}}</h3>
                      <div class="cke">
                        {!! $produto->traduz('ingredientes') !!}
                      </div>
                    </div>
                    <div class="tabela">
                      <table>
                        <thead>
                          <tr>
                            <th colspan="3">
                              {{trans('site.tabela_nutri.titulo')}}
                              <br>
                              {{$produto->traduz('tabela_porcao')}}
                            </th>
                          </tr>
                          <tr>
                            <th colspan="2">{{trans('site.tabela_nutri.quantidade')}}</th>
                            <th>{{trans('site.tabela_nutri.vd')}}</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td>{{trans('site.tabela_nutri.valor_calorico')}}</td>
                            <td>{{$produto->nutri_valor_calorico}}</td>
                            <td>{{$produto->nutri_valor_calorico_pct}}</td>
                          </tr>
                          <tr>
                            <td>{{trans('site.tabela_nutri.carboidratos')}}</td>
                            <td>{{$produto->nutri_carboidratos}}</td>
                            <td>{{$produto->nutri_carboidratos_pct}}</td>
                          </tr>
                          <tr>
                            <td>{{trans('site.tabela_nutri.proteinas')}}</td>
                            <td>{{$produto->nutri_proteinas}}</td>
                            <td>{{$produto->nutri_proteinas_pct}}</td>
                          </tr>
                          <tr>
                            <td>{{trans('site.tabela_nutri.gorduras_totais')}}</td>
                            <td>{{$produto->nutri_gorduras_totais}}</td>
                            <td>{{$produto->nutri_gorduras_totais_pct}}</td>
                          </tr>
                          <tr>
                            <td>{{trans('site.tabela_nutri.gorduras_saturadas')}}</td>
                            <td>{{$produto->nutri_gorduras_saturadas}}</td>
                            <td>{{$produto->nutri_gorduras_saturadas_pct}}</td>
                          </tr>
                          <tr>
                            <td>{{trans('site.tabela_nutri.gorduras_trans')}}</td>
                            <td>{{$produto->nutri_gorduras_trans}}</td>
                            <td>{{$produto->nutri_gorduras_trans_pct}}</td>
                          </tr>
                          <tr>
                            <td>{{trans('site.tabela_nutri.fibra_alimentar')}}</td>
                            <td>{{$produto->nutri_fibra_alimentar}}</td>
                            <td>{{$produto->nutri_fibra_alimentar_pct}}</td>
                          </tr>
                          <tr>
                            <td>{{trans('site.tabela_nutri.sodio')}}</td>
                            <td>{{$produto->nutri_sodio}}</td>
                            <td>{{$produto->nutri_sodio_pct}}</td>
                          </tr>
                        </tbody>
                        <tfoot>
                          <tr>
                            <td colspan="3">{{trans('site.tabela_nutri.obs')}}</td>
                          </tr>
                        </tfoot>
                      </table>
                    </div>
                  </div>
                </div>
              </div>
            @endforeach
          </div>
        </div>

        <a style="background-color:{{$tipoSelecionado->cor_fundo}}" href="nossas-linhas/{{$linhaSelecionada->slug_pt}}" title="{{ trans('site.detalhes.voltar') . $linhaSelecionada->traduz('titulo')}}" class="link-linha-fullw">{{ trans('site.detalhes.voltar') . $linhaSelecionada->traduz('titulo')}} &raquo;</a>
      </div>

    @endif

    @if(!$tipoSelecionado)
      <div class="banner-linhas">
        <div class="centralizar">
          <div class="chamada">{{conteudo('banner_linhas.frase1')}}</div>
          <div class="titulo">{{conteudo('banner_linhas.frase2')}}</div>
          <div class="subtitulo">{{conteudo('banner_linhas.frase3')}}</div>
          <div class="links">
            @foreach($listaLinhas as $linha)
              <a href="nossas-linhas/{{$linha->slug_pt}}" title="{{$linha->traduz('titulo')}}">
                <div class="imagem" style="background:url('assets/img/linhas/{{$linha->imagem_todaslinhas}}')center center no-repeat;">
                  <img src="assets/img/linhas/{{$linha->imagem_todaslinhas}}" />
                </div>
                <div class="texto">
                  {{ conteudo('banner_linhas.'.$linha->slug_pt.'.texto') }}
                </div>
                <div class="botao">VER PRODUTOS &raquo;</div>
              </a>
            @endforeach
          </div>
        </div>
      </div>
    @endif

  </div>

@stop
