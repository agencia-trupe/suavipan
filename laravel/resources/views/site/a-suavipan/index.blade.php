@extends('site.template')

@section('conteudo')

  <div class="conteudo conteudo-a-suavipan">
    <div class="centralizar">
      <div class="coluna">
        <h1>{{conteudo('quemsomos.titulo')}}</h1>
        <div class="cke">{!! conteudo('quemsomos.texto') !!}</div>
      </div>
      <div class="coluna">
        <div class="coluna reduzida">
          <div class="box">
            <h2>{{trans('site.quemsomos.visao')}}</h2>
            <div class="cke">{!! conteudo('quemsomos.visao') !!}</div>
          </div>
          <div class="box">
            <h2>{{trans('site.quemsomos.missao')}}</h2>
            <div class="cke">{!! conteudo('quemsomos.missao') !!}</div>
          </div>
        </div>
        <div class="coluna aumentada">
          <div class="box">
            <h2>{{trans('site.quemsomos.valores')}}</h2>
            <div class="cke">{!! conteudo('quemsomos.valores') !!}</div>
          </div>
          <div class="associacao">
            {{trans('site.quemsomos.associacao')}} <img src="assets/img/layout/marca-associado-abimapi.png" alt="Abimapi">
          </div>
        </div>
      </div>
    </div>
  </div>

@stop
