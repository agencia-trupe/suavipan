@extends('site.template')

@section('conteudo')
    <div id="backg"></div>
    <div id="centro">
        <div class="close">
            <a onclick="ocultar()">X</a>
        </div>
        <a href="https://loja.suavipan.com.br"><img src="assets/img/layout/popupsuavipan.jpg" class="imgpopup" /></a>
    </div>

        <div class="conteudo conteudo-home">

    <div id="banners-home">

      <div id="banners"
        class="cycle-slideshow"
        data-cycle-slides="> .banner"
        data-cycle-log="false"
        data-cycle-pager="#banners-pager .pager"
        data-cycle-starting-slide="4"
        data-cycle-pager-template
        data-cycle-pager-active-class="slide-ativo"
        >
        <div class="banner banner-integral">
          <div class="centralizar">
            <div class="texto">
              <div class="frase1">{{conteudo('chamada_home.integral.frase1')}}</div>
              <div class="frase2">{{conteudo('chamada_home.integral.frase2')}}</div>
              <div class="frase3">{{conteudo('chamada_home.integral.frase3')}}</div>
            </div>
            <div class="imagem">
              <img src="assets/img/conteudos/{{conteudo('chamada_home.integral.imagem')}}" alt="{{trans('linha_organico')}}" />
            </div>
            <div class="bolinhas">
              <div class="bolinha integral-gorduras-trans"></div>
              <div class="bolinha integral-farinhas-integrais"></div>
              <div class="bolinha integral-zero-lactose"></div>
              <div class="bolinha integral-fibras"></div>
              <div class="bolinha integral-oleo-canola"></div>
              <div class="bolinha integral-acucar-mascavo"></div>
            </div>
          </div>
        </div>
        <div class="banner banner-levslim">
          <div class="centralizar">
            <div class="texto">
              <div class="frase1">{{conteudo('chamada_home.levslim.frase1')}}</div>
              <div class="frase2">{{conteudo('chamada_home.levslim.frase2')}}</div>
              <div class="frase3">{{conteudo('chamada_home.levslim.frase3')}}</div>
            </div>
            <div class="imagem">
              <img src="assets/img/conteudos/{{conteudo('chamada_home.levslim.imagem')}}" alt="{{trans('linha_organico')}}" />
            </div>
            <div class="bolinhas">
              <div class="bolinha levslim-gorduras-trans"></div>
              <div class="bolinha levslim-zero-acucar"></div>
            </div>
          </div>
        </div>
        <div class="banner banner-organico">
          <div class="centralizar">
            <div class="texto">
              <div class="frase1">{{conteudo('chamada_home.organico.frase1')}}</div>
              <div class="frase2">{{conteudo('chamada_home.organico.frase2')}}</div>
              <div class="frase3">{{conteudo('chamada_home.organico.frase3')}}</div>
            </div>
            <div class="imagem">
              <img src="assets/img/conteudos/{{conteudo('chamada_home.organico.imagem')}}" alt="{{trans('linha_organico')}}" />
            </div>
            <div class="bolinhas">
              <div class="bolinha organico-nutricional"></div>
              <div class="bolinha organico-sem-agrotoxicos"></div>
              <div class="bolinha organico-meio-ambiente"></div>
              <div class="bolinha organico-mais-saude"></div>
              <div class="bolinha organico-ecocert"></div>
            </div>
          </div>
        </div>
        <div class="banner banner-zero">
          <div class="centralizar">
            <div class="texto">
              <div class="frase1">{{conteudo('chamada_home.zero.frase1')}}</div>
              <div class="frase2">{{conteudo('chamada_home.zero.frase2')}}</div>
              <div class="frase3">{{conteudo('chamada_home.zero.frase3')}}</div>
            </div>
            <div class="imagem">
              <img src="assets/img/conteudos/{{conteudo('chamada_home.zero.imagem')}}" alt="{{trans('linha_organico')}}" />
            </div>
            <div class="bolinhas">
              <div class="bolinha zero-zero-acucar"></div>
              <div class="bolinha zero-fibras"></div>
              <div class="bolinha zero-baixas-calorias"></div>
              <div class="bolinha zero-baixo-sodio"></div>
              <div class="bolinha zero-gorduras-trans"></div>
            </div>
          </div>
        </div>
        <div class="banner banner-linhacompleta">
          <div class="centralizar">
            <div class="texto">
              <div class="frase1">{{conteudo('chamada_home.linhacompleta.frase1')}}</div>
              <div class="frase2">{{conteudo('chamada_home.linhacompleta.frase2')}}</div>
              <div class="frase3">{{conteudo('chamada_home.linhacompleta.frase3')}}</div>
            </div>
            <div class="imagem">
              <img src="assets/img/conteudos/{{conteudo('chamada_home.linhacompleta.imagem')}}" alt="{{trans('linha_organico')}}" />
            </div>
            <div class="bolinhas">
              <div class="bolinha linhacompleta-bolos"></div>
              <div class="bolinha linhacompleta-bolinhos"></div>
              <div class="bolinha linhacompleta-alfajor"></div>
              <div class="bolinha linhacompleta-muffins"></div>
              <div class="bolinha linhacompleta-pao-de-mel"></div>
            </div>
          </div>
        </div>
      </div>

      <div id="banners-pager">
        <div class="centralizar">
          <ul class="pager">
            <li class='pager-integral' style="background-image: url('assets/img/linhas/{{$linha_integral->imagem_home_off}}')">
              <a href="#" style="background-image: url('assets/img/linhas/{{$linha_integral->imagem_home_on}}')"></a>
            </li>
            <li class='pager-levslim' style="background-image: url('assets/img/linhas/{{$linha_levslim->imagem_home_off}}')">
              <a href="#" style="background-image: url('assets/img/linhas/{{$linha_levslim->imagem_home_on}}')"></a>
            </li>
            <li class='pager-organico' style="background-image: url('assets/img/linhas/{{$linha_organico->imagem_home_off}}')">
              <a href="#" style="background-image: url('assets/img/linhas/{{$linha_organico->imagem_home_on}}')"></a>
            </li>
            <li class='pager-zero' style="background-image: url('assets/img/linhas/{{$linha_zero->imagem_home_off}}')">
              <a href="#" style="background-image: url('assets/img/linhas/{{$linha_zero->imagem_home_on}}')"></a>
            </li>
            <li class='pager-linhacompleta' style="background-image: url('assets/img/conteudos/{{conteudo('banner_suavipan.imagem_off')}}')">
              <a href="#" style="background-image: url('assets/img/conteudos/{{conteudo('banner_suavipan.imagem_on')}}')"></a>
            </li>
          </ul>
        </div>
      </div>

    </div>

    <div class="centralizar">

    </div>

  </div>

@stop
