<nav>
  <div class="centralizar">

    <a href="{{URL::route('home')}}" id="link-home" title="{{ trans('site.menu.pagina-inicial') }}">
      <img src="assets/img/layout/marca-suavipan.png" alt="Suavipan" />
    </a>

    <a href="#" id="menu-toggle-mobile">MENU</a>

    <ul>
      <li>
        <a href="{{URL::route('todos-produtos')}}" @if(str_is('todos-produtos*', Route::currentRouteName())) class="ativo" @endif title="{{ trans('site.menu.todos-produtos') }}">{{ trans('site.menu.todos-produtos') }}</a>
      </li>
      <li>
        <a href="{{URL::route('nossas-linhas')}}" @if(str_is('nossas-linhas*', Route::currentRouteName())) class="ativo" @endif title="{{ trans('site.menu.nossas-linhas') }}">{{ trans('site.menu.nossas-linhas') }}</a>
      </li>
      <li>
        <a href="{{URL::route('a-suavipan')}}" @if(str_is('a-suavipan*', Route::currentRouteName())) class="ativo" @endif title="{{ trans('site.menu.a-suavipan') }}">{{ trans('site.menu.a-suavipan') }}</a>
      </li>
      <li>
        <a href="{{URL::route('contato')}}" @if(str_is('contato*', Route::currentRouteName())) class="ativo" @endif title="{{ trans('site.menu.contato') }}">{{ trans('site.menu.contato') }}</a>
      </li>
      <li>
        <a href="https://loja.suavipan.com.br"  title="Loja Online">Loja Online</a>
      </li>
    </ul>

    <div class="menu-secundario">
      <div class="social">
        <a href="{{conteudo('contato.facebook')}}" target="_blank" title="Facebook"><img src="assets/img/layout/icone-facebook.png" alt="Facebook" /></a>
        <a href="{{conteudo('contato.instagram')}}" target="_blank" title="Instagram"><img src="assets/img/layout/icone-instagram-2.png" alt="Instagram" /></a>
      </div>
      <div class="localizacao">
        <!--
        <a href="idioma/pt-br" title="{{ trans('site.menu.versao-pt-br') }}"><img src="assets/img/layout/icone-idioma-pt.png" alt="{{ trans('site.menu.versao-pt-br') }}" /></a>
        <a href="idioma/es" title="{{ trans('site.menu.versao-es') }}"><img src="assets/img/layout/icone-idioma-es.png" alt="{{ trans('site.menu.versao-es') }}" /></a>
        <a href="idioma/en" title="{{ trans('site.menu.versao-en') }}"><img src="assets/img/layout/icone-idioma-en.png" alt="{{ trans('site.menu.versao-en') }}" /></a>
        -->
      </div>
    </div>

  </div>
</nav>
