<div id="compartilhe" @if(str_is('home', Route::currentRouteName())) class="compartilhe-home" @endif>
  <div class="centralizar">

    <div class="visite-loja">
      <h2>VISITE NOSSA LOJA <br> ONLINE E APROVEITE!</h2>
      <!--<a href="https://loja.suavipan.com.br" title="Loja Online"><img src="assets/img/layout/img-cesta.png" alt="loja Online" style="width: 100px;" /></a> -->
      <a href="https://loja.suavipan.com.br" title="Loja Online"><img src="assets/img/layout/cestinha-azul.png" alt="Loja Online" /></a>
    </div>

    <div class="newsletter">
      <h2>{{trans('site.form-newsletter.titulo')}}</h2>
      @if(session('newsletter_sucesso'))
      <div class="form-resposta">
        {{trans('site.form-newsletter.resposta')}}
      </div>
      @endif
      @if($errors->has('newsletter.nome') || $errors->has('newsletter.email'))
      <div class="form-resposta erro">
        {{$errors->first()}}
      </div>
      @endif
      <form action="{{URL::route('cadastro.newsletter')}}" method="post" novalidate>
        {!!csrf_field()!!}
        <input type="text" name="newsletter[nome]" placeholder="{{trans('site.form.nome')}}" required value="{{old('newsletter.nome')}}">
        <input type="email" name="newsletter[email]" placeholder="{{trans('site.form.email')}}" required value="{{old('newsletter.email')}}">

        <div class="contato-opt-in">
          <input type="checkbox" id="optInNewsletter" name="optIn" value="1" required>
          <p class="texto-lgpd">Aceito que ao fornecer meus dados para contato a Suavipan os utilizará apenas para responder minha solicitação ou fazer marketing da própria empresa (divulgação de conteúdo, serviços ou produtos relevantes). Meus dados não serão compartilhados ou utilizados para outros fins. Estou ciente de que poderei me excluir do mailing da empresa a qualquer momento através do link localizado nos e-mails enviados, nos termos da Lei 13.709/18 – LGPD.</p>
        </div>

        <input type="submit" class="btn-enviar-newsletter" value="{{trans('site.form.enviar')}}">
      </form>
    </div>

    <div class="contato-social">
      <h2>{{trans('site.compartilhe.titulo')}}</h2>
      <a href="{{conteudo('contato.facebook')}}" title="Facebook"><img src="assets/img/layout/icone-facebook-gde.png" alt="Facebook" /></a>
      <a href="{{conteudo('contato.instagram')}}" title="Instagram"><img src="assets/img/layout/icone-instagram-gde-2.png" alt="Instagram" /></a>
      <a href="mailto:suavipan@suavipan.com.br" title="E-mail"><img src="assets/img/layout/icone-email-gde.png" alt="E-mail" /></a>
    </div>

  </div>
</div>