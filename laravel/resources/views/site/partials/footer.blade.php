<footer>

  <div class="centralizar">

    <div class="lista-galerias">

      <div>
        <a href="{{URL::route('todos-produtos')}}" class="item-menu item-menu-desktop" title="{{trans('site.menu.produtos')}}">{{trans('site.menu.produtos')}}</a>
      </div>

      <ul>
        @if($linha_integral)
          <li class='titulo-linha'>
            <a href="nossas-linhas/integral" title="{{$linha_integral->traduz('titulo')}}">Linha {{$linha_integral->traduz('titulo')}}</a>
          </li>
          @foreach($linha_integral->tipos as $tipo_integral)
            @foreach($tipo_integral->produtos as $produto_integral)
              
              <li>
                <a href="nossas-linhas/integral/{{$tipo_integral->slug_pt}}#{{$produto_integral->slug_pt}}" title="{{$tipo_integral->traduz('titulo').' '.$produto_integral->traduz('titulo')}}">{{$tipo_integral->traduz('titulo').' '.$produto_integral->traduz('titulo')}}</a>
              </li>
            @endforeach
          @endforeach
        @endif
        
        @if($linha_organico)
          <li class='titulo-linha'>
            <a href="nossas-linhas/organico" title="{{$linha_organico->traduz('titulo')}}">Linha {{$linha_organico->traduz('titulo')}}</a>
          </li>
          @foreach($linha_organico->tipos as $tipo_organico)
            @foreach($tipo_organico->produtos as $produto_organico)
              <li>
                <a href="nossas-linhas/organico/{{$tipo_organico->slug_pt}}#{{$produto_organico->slug_pt}}" title="{{$tipo_organico->traduz('titulo').' '.$produto_organico->traduz('titulo')}}">{{$tipo_organico->traduz('titulo').' '.$produto_organico->traduz('titulo')}}</a>
              </li>
            @endforeach
          @endforeach
        @endif
        
      </ul>
      <ul>
        @if($linha_levslim)
          <li class='titulo-linha'>
            <a href="nossas-linhas/levslim" title="{{$linha_levslim->traduz('titulo')}}">Linha {{$linha_levslim->traduz('titulo')}}</a>
          </li>
          @foreach($linha_levslim->tipos as $tipo_levslim)
            @foreach($tipo_levslim->produtos as $produto_levslim)
            
                <li>
                <a href="nossas-linhas/levslim/{{$tipo_levslim->slug_pt}}#{{$produto_levslim->slug_pt}}" title="{{$tipo_levslim->traduz('titulo').' '.$produto_levslim->traduz('titulo')}}">{{$tipo_levslim->traduz('titulo').' '.$produto_levslim->traduz('titulo')}}</a>
                </li>
              
              
            @endforeach
          @endforeach
        @endif

        
      </ul>
      <ul>
        @if($linha_zero)
          <li class='titulo-linha'>
            <a href="nossas-linhas/zero" title="{{$linha_zero->traduz('titulo')}}">Linha {{$linha_zero->traduz('titulo')}}</a>
          </li>
          @foreach($linha_zero->tipos as $tipo_zero)
            @foreach($tipo_zero->produtos as $produto_zero)
              <li>
                <a href="nossas-linhas/zero/{{$tipo_zero->slug_pt}}#{{$produto_zero->slug_pt}}" title="{{$tipo_zero->traduz('titulo').' '.$produto_zero->traduz('titulo')}}">{{$tipo_zero->traduz('titulo').' '.$produto_zero->traduz('titulo')}}</a>
              </li>
            @endforeach
          @endforeach
        @endif
      </ul>
    </div>

    <div class="menu-footer">

      <a href="{{URL::route('a-suavipan')}}" class="item-menu" title="{{trans('site.menu.a-suavipan')}}">{{trans('site.menu.a-suavipan')}}</a>
      <a href="https://loja.suavipan.com.br" class="item-menu" title="Loja Online">Loja Online</a>
      <a href="{{URL::route('contato')}}" class="item-menu" title="{{trans('site.menu.contato')}}">{{trans('site.menu.contato')}}</a>
      <a href="{{URL::route('politica-de-privacidade')}}" class="item-menu" title="Política de Privacidade" style="font-weight: 500; font-size: 12px;">Política de Privacidade</a>

      <div class="contato-footer">
          <div class="telefone">
              11 2096-7069
          </div>
        <div class="telefone">
          {{conteudo('contato.telefone')}}
        </div>
        <div class="telefone">
          {{conteudo('contato.telefone_sac')}}
        </div>
        <div class="email">
          <a href="mailto:{{conteudo('contato.email')}}" title="{{trans('site.menu.entre-em-contato')}}">{{conteudo('contato.email')}}</a>
        </div>
        <div class="social">
          <a href="{{conteudo('contato.facebook')}}" target="_blank" title="Facebook"><img src="assets/img/layout/icone-facebook-gde.png" alt="Facebook" /></a>
          <a href="{{conteudo('contato.instagram')}}" target="_blank" title="Instagram"><img src="assets/img/layout/icone-instagram-gde-2.png" alt="Instagram" /></a>
        </div>
      </div>

    </div>

  </div>

  <div class="assinatura">
    &copy; {{date('Y')}} &middot; Suavipan &middot; {{trans('site.menu.direitos')}} | <a href="http://www.trupe.net" target="_blank" title="Criação de sites: Trupe Agência Criativa">Criação de sites: Trupe Agência Criativa</a>
  </div>

</footer>
