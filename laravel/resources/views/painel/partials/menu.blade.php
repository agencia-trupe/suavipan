<nav class="navbar navbar-default">
	<div class="container-fluid">

		<div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#menu-painel" aria-expanded="false">
				<span class="sr-only">Navegação</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<a class="navbar-brand" title="Página Inicial" href="{{ URL::route('painel.dashboard') }}">Suavipan</a>
		</div>

		<div class="collapse navbar-collapse" id="menu-painel">
			<ul class="nav navbar-nav">

				<li @if(str_is('painel.dashboard*', Route::currentRouteName())) class="active" @endif>
					<a href="{{URL::route('painel.dashboard')}}" title="Início">Início</a>
				</li>

				<!--RESOURCEMENU-->
				<li @if(str_is('painel.home*', Route::currentRouteName())) class="active" @endif>
					<a href="{{URL::route('painel.home.index')}}" title="Home">Home</a>
				</li>

				<li @if(str_is('painel.todos-os-produtos*', Route::currentRouteName())) class="active" @endif>
					<a href="{{URL::route('painel.todos-os-produtos.index')}}" title="Todos os Produtos">Todos os Produtos</a>
				</li>

				<li @if(str_is('painel.nossas-linhas*', Route::currentRouteName())) class="active" @endif>
					<a href="{{URL::route('painel.nossas-linhas.index')}}" title="Nossas Linhas">Nossas Linhas</a>
				</li>

				<li @if(str_is('painel.a-suavipan*', Route::currentRouteName())) class="active" @endif>
					<a href="{{URL::route('painel.a-suavipan.index')}}" title="A Suavipan">A Suavipan</a>
				</li>

				<li @if(str_is('painel.contato*', Route::currentRouteName())) class="active" @endif>
					<a href="{{URL::route('painel.contato.index')}}" title="Informações de Contato">Informações de Contato</a>
				</li>

				<li @if(preg_match('/painel.(linhas*|tipos*|produtos*)/', Route::currentRouteName())) class="active" @endif>
					<a href="{{URL::route('painel.linhas.index')}}" title="Catálogo de Produtos">Catálogo de Produtos</a>
				</li>

				<li @if(preg_match('/painel.(cadastros*)/', Route::currentRouteName())) class="active" @endif>
					<a href="{{URL::route('painel.cadastros.index')}}" title="Cadastros">Cadastros</a>
				</li>

				<li @if(preg_match('/painel.(descadastros*)/', Route::currentRouteName())) class="active" @endif>
					<a href="{{URL::route('painel.descadastros.index')}}" title="Descadastros">Descadastros</a>
				</li>

				<li class="dropdown @if(preg_match('~painel.(usuarios|idiomas*)~', Route::currentRouteName())) active @endif ">

					<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
						<span class="glyphicon glyphicon-cog small" style="margin-right:3px"></span>
						<b class="caret"></b>
					</a>

					<ul class="dropdown-menu">
						<li @if(preg_match('/painel.(politica-de-privacidade*)/', Route::currentRouteName())) class="active" @endif>
							<a href="{{URL::route('painel.politica-de-privacidade.index')}}" title="Política de Privacidade">Política de Privacidade</a>
						</li>
						<li @if(preg_match('~painel.(aceite-de-cookies*)~', Route::currentRouteName())) class="active" @endif>
							<a href="{{URL::route('painel.aceite-de-cookies')}}" title="Aceite de Cookies">Aceite de Cookies</a>
						</li>
						<li @if(preg_match('~painel.(usuarios*)~', Route::currentRouteName())) class="active" @endif>
							<a href="{{URL::route('painel.usuarios.index')}}" title="Usuários do Painel">Usuários</a>
						</li>
						<li role="separator" class="divider"></li>
						<li>
							<a href="{{URL::route('painel.logout')}}" title="Logout">Logout</a>
						</li>
					</ul>
				</li>

			</ul>
		</div>

	</div>
</nav>