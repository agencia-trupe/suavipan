<hr>

<div class="well">

	<a href="#" class="btn btn-sm btn-success pull-right" id="adicionarArquivo"><span class="glyphicon glyphicon-plus-sign"></span> adicionar arquivo</a>
	
	<label style='display:block;'>Arquivos </label>

	<div class='list-group' id="listaArquivosExistentes" style='clear:right; margin: 15px 0;'>
		@if(isset($registro) && sizeof($registro->arquivos))
			@foreach($registro->arquivos as $arquivo)
				<div class='list-group-item'>
		      <div class='form-group'>
						<a href="assets/arquivos/{{$arquivo->arquivo}}" class="btn btn-sm btn-default" target="_blank"><span class="glyphicon glyphicon-file"></span> {{$arquivo->titulo}}</a>
						<input type="hidden" name="arquivos_ids[]" value="{{$arquivo->id}}">
		      </div>
		      <div class='form-group'>
		        <a href='#' class='btn btn-sm btn-danger btn-remover-arquivo'>remover este arquivo</a>
		      </div>
		    </div>
			@endforeach
		@endif
  </div>

  <div class='list-group' id="listaArquivos" style='clear:right; margin: 15px 0;'>

  </div>

  <div class="panel panel-default">
		<div class="panel-body">
	    	Você pode clicar e arrastar os arquivos para ordená-los.
	  	</div>
	</div>

</div>
