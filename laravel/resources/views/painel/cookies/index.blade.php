@extends('painel.template')

@section('conteudo')

<div class="container-fluid padded-bottom">
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

            <h2>Relatório de Aceite de Cookies</h2>

            <hr>

            @include('painel.partials.mensagens')

            <table class="table table-striped table-bordered table-hover">
                <thead>
                    <tr>
                        <th>Data</th>
                        <th>IP</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($registros as $registro)
                    <tr class="tr-row">
                        <td>
                            {{ strftime("%d/%m/%Y %H:%M:%S", strtotime($registro->created_at)) }}
                        </td>
                        <td>
                            {{ $registro->ip }}
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>

@stop