@extends('painel.template')

@section('conteudo')

<div class="container-fluid padded-bottom">
  <div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

      <h2>Política de Privacidade</h2>
      <hr>

      @include('painel.partials.mensagens')

      <form action="{{ URL::route('painel.politica-de-privacidade.update', $registro->id) }}" method="post" enctype="multipart/form-data">
        {!! csrf_field() !!}
        <input type="hidden" name="_method" value="PUT">
        <div class="form-group">
          <label>Texto</label>
          <textarea class="form-control" name="texto">{{ $registro->texto }}</textarea>
        </div>
        <button type="submit" title="Alterar" class="btn btn-success">Alterar</button>
      </form>

    </div>
  </div>
</div>

@endsection