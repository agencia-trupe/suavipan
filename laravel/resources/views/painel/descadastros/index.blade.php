@extends('painel.template')

@section('conteudo')

<div class="container-fluid padded-bottom">
  <div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

      <h2>Cadastros da Newsletter</h2>

      <hr>

      @include('painel.partials.mensagens')

      <a href="{{ URL::route('painel.descadastros.download') }}" class="btn btn-success btn-sm"><span class="glyphicon glyphicon-circle-arrow-down"></span> EXTRAIR ARQUIVO XLS</a>

      <table class="table table-striped table-bordered table-hover">

        <thead>
          <tr>
            <th>Data</th>
            <th>E-mail</th>
            <th><span class="glyphicon glyphicon-cog"></span></th>
          </tr>
        </thead>

        <tbody>
          @foreach ($registros as $registro)
            <tr class="tr-row">
              <td>
                {{ $registro->created_at->format('d/m/Y H:i') }}
              </td>
              <td>
                {{ $registro->email }}
              </td>
              <td class="crud-actions">
                <form action="{{ URL::route('painel.descadastros.destroy', $registro->id) }}" method="post">
                  {!! csrf_field() !!}
                  <input type="hidden" name="_method" value="DELETE">
                  <button type="submit" class="btn btn-danger btn-sm btn-delete">excluir</button>
                </form>
              </td>
            </tr>
          @endforeach
        </tbody>

      </table>
    </div>
  </div>
</div>

@endsection
