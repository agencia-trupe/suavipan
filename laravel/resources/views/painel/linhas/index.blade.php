@extends('painel.template')

@section('conteudo')

<div class="container-fluid padded-bottom">
  <div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

      <h2>Linhas de Produtos</h2>

      <hr>

      @include('painel.partials.mensagens')
      
      <a href="{{ URL::route('painel.linhas.create') }}" class="btn btn-success btn-sm"><span class="glyphicon glyphicon-plus-sign"></span> Adicionar Linha</a>

      <table class="table table-striped table-bordered table-hover">

        <thead>
          <tr>
            <th>Título</th>
            <th></th>
            <th><span class="glyphicon glyphicon-cog"></span></th>
          </tr>
        </thead>

        <tbody>
          @foreach ($registros as $registro)

            <tr class="tr-row" id="row_{{ $registro->id }}">
              <td>{{ $registro->titulo_pt }}</td>
              <td><a href="{{URL::route('painel.tipos.index', ['linha_id' => $registro->id])}}" class="btn btn-sm btn-default">Tipos de Produtos</a></td>
              <td class="crud-actions">
                <a href="{{ URL::route('painel.linhas.edit', $registro->id ) }}" class="btn btn-primary btn-sm">editar</a>
              </td>
            </tr>
          @endforeach
        </tbody>

      </table>
    </div>
  </div>
</div>

@endsection
