@extends('painel.template')

@section('conteudo')

  <div class="container-fluid padded-bottom">
  	<div class="row">
  		<div class="col-xs-12 col-sm-8 col-md-6 col-lg-4">

        <h2>Linha: {{$linha->titulo_pt}}</h2>

		    <h3>Inserir Tipo de Produto</h3>

        <hr>

        <a href="{{URL::route('painel.tipos.index', ['linha_id' => $linha->id])}}" class="btn btn-default">&larr; voltar</a>

		    <hr>

		    @include('painel.partials.mensagens')

		    <form action="{{ URL::route('painel.tipos.store') }}" method="post" enctype="multipart/form-data">
		    
		     <input type="hidden" name="linhas_id" value="{{$linha->id}}" />

					{!! csrf_field() !!}

			  	<div class="form-group">
					  <label>Título em Português</label>
						<input type="text" class="form-control" name="titulo_pt"  required>
					</div>
          <div class="form-group">
					  <label>Título em Inglês</label>
						<input type="text" class="form-control" name="titulo_en" >
					</div>
          <div class="form-group">
					  <label>Título em Espanhol</label>
						<input type="text" class="form-control" name="titulo_es" >
					</div>

					<hr>

          <div class="form-group">
					  <label>Chamada em Português</label>
						<textarea class="form-control" name="chamada_pt"></textarea>
					</div>
          <div class="form-group">
					  <label>Chamada em Inglês</label>
						<textarea class="form-control" name="chamada_en"></textarea>
					</div>
          <div class="form-group">
					  <label>Chamada em Espanhol</label>
						<textarea class="form-control" name="chamada_es"></textarea>
					</div>

          <hr>

          <div class="form-group">
					  <label>Cor</label>
						<input type="text" class="form-control" name="cor_fundo" >
					</div>

          <hr>

          <div class="form-group">
            <label>Alterar Imagem</label>
            <input type="file" class="form-control" name="imagem">
          </div>
          
         

					<button type="submit" title="Alterar" class="btn btn-success">Alterar</button>

					<a href="{{ URL::route('painel.tipos.index')}}" title="Voltar" class="btn btn-default btn-voltar">Voltar</a>

				</form>

      </div>
		</div>
  </div>

@endsection
