@extends('painel.template')

@section('conteudo')

<div class="container-fluid padded-bottom">
  <div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

      <h2>Linha: {{$linha->titulo_pt}}</h2>

      <h3>Tipos de Produtos</h3>

      <hr>

      <a href="{{URL::route('painel.linhas.index')}}" class="btn btn-default">&larr; voltar</a>

      <hr>

      @include('painel.partials.mensagens')
      
      <a href="{{ URL::route('painel.tipos.create', ['linha_id' => $linha->id])}}" class="btn btn-success btn-sm"><span class="glyphicon glyphicon-plus-sign"></span> Adicionar Tipo</a>

      <table class="table table-striped table-bordered table-hover table-sortable" data-tabela='tipos'>

        <thead>
          <tr>
            <th>Ordenar</th>
            <th>Título</th>
            <th></th>
            <th><span class="glyphicon glyphicon-cog"></span></th>
          </tr>
        </thead>

        <tbody>
          @foreach ($linha->tipos as $registro)

            <tr class="tr-row" id="row_{{ $registro->id }}">
              <td class='move-actions'><a href='#' class='btn btn-info btn-move btn-sm'>mover</a></td>
              <td>{{ $registro->titulo_pt }}</td>
              <td><a href="{{URL::route('painel.produtos.index', ['linha_id' => $linha->id, 'tipo_id' => $registro->id])}}" class="btn btn-sm btn-default">Produtos</a></td>
              <td class="crud-actions">
                <a href="{{ URL::route('painel.tipos.edit', $registro->id ) }}" class="btn btn-primary btn-sm">editar</a>
              </td>
            </tr>
          @endforeach
        </tbody>

      </table>
    </div>
  </div>
</div>

@endsection
