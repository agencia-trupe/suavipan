@extends('painel.template')

@section('conteudo')

<div class="container-fluid padded-bottom">
  <div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

      <h2>Conteúdos</h2>

      <hr>

      @include('painel.partials.mensagens')

      <form action="{{ URL::route('painel.conteudos.update') }}" method="post" enctype="multipart/form-data">

        {!! csrf_field() !!}

        <table class="table table-striped table-bordered table-hover ">

          <thead>
            <tr>
              <th>Campo</th>
              <th>Valor - português</th>
              <th>Valor - inglês</th>
              <th>Valor - espanhol</th>
            </tr>
          </thead>

          <tbody>
            @foreach ($registros as $registro)

              <tr class="tr-row">
                <td>
                  {{ $registro->indice }}
                </td>
                @if($registro->tipo_campo == 'texto')
                  <td>
                    <textarea name="conteudos[{{$registro->indice}}][valor_pt_br]" class="form-control">{{$registro->valor_pt_br}}</textarea>
                  </td>
                  <td>
                    <textarea name="conteudos[{{$registro->indice}}][valor_en]" class="form-control">{{$registro->valor_en}}</textarea>
                  </td>
                  <td>
                    <textarea name="conteudos[{{$registro->indice}}][valor_es]" class="form-control">{{$registro->valor_es}}</textarea>
                  </td>
                @elseif($registro->tipo_campo == 'imagem')
                  <td colspan="3">
                    <div style='text-align:center; padding: 20px 0;'>
                      @if($registro->valor_pt_br)
                        <img src="assets/img/conteudos/{{$registro->valor_pt_br}}" style='display:block; margin: 0 auto 5px auto;' />
                      @endif
                      <input style='display:inline;' type="file" name="arquivos[{{$registro->indice}}]">
                    </div>
                  </td>
                @elseif($registro->tipo_campo == 'string')
                  <td>
                    <textarea name="conteudos[{{$registro->indice}}][valor_pt_br]" class="form-control textarea-simples">{{$registro->valor_pt_br}}</textarea>
                  </td>
                  <td>
                    <textarea name="conteudos[{{$registro->indice}}][valor_en]" class="form-control textarea-simples">{{$registro->valor_en}}</textarea>
                  </td>
                  <td>
                    <textarea name="conteudos[{{$registro->indice}}][valor_es]" class="form-control textarea-simples">{{$registro->valor_es}}</textarea>
                  </td>
                @endif
              </tr>
            @endforeach
          </tbody>

        </table>

        <input type="submit" value="Gravar" class="btn btn-success">

      </form>

    </div>
  </div>
</div>

@endsection
