<!DOCTYPE html>
<html lang="pt-BR" class="no-js">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="robots" content="index, nofollow" />
    <meta name="author" content="Trupe Design" />
    <meta name="copyright" content="2013 Trupe Design" />
    <meta name="viewport" content="width=device-width,initial-scale=1">

    <meta name="csrf-token" content="{{ csrf_token() }}" />

    <title>SuaviPan - Painel Administrativo</title>

	<base href="{{ base_url() }}/">
	<script>var BASE = "{{ base_url() }}"</script>

	<link rel="stylesheet" href="assets/css/painel.css">

	<!-- <script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.0/jquery.min.js"></script> -->
	<script src="assets/js/jquery.js"></script>
	<script>window.jQuery || document.write('<script src="assets/vendor/jquery/dist/jquery.min.js"><\/script>')</script>

	<script src='assets/js/modernizr.js'></script>

</head>
	<body class="body-painel">

		@include('painel.partials.menu')

		@yield('conteudo')

		<footer>
			<div class="container-fluid">
				<a href="http://www.trupe.net" target="_blank" title="Criação de Sites : Trupe Agência Criativa">© Criação de Sites : Trupe Agência Criativa</a>
			</div>
		</footer>

		<script src='assets/js/ckeditor/ckeditor.js'></script>
		<script src='assets/js/ckeditor/adapters/jquery.js'></script>
		<script src='assets/js/painel.js?nocache={{ time() }}'></script>

	</body>
</html>
