@extends('painel.template')

@section('conteudo')

<div class="container-fluid padded-bottom">
  <div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

      <h2>Linha: {{$linha->titulo_pt}}</h2>

      <h2>Tipo: {{$tipo->titulo_pt}}</h2>

      <h3>Produtos</h3>

      <hr>

      <a href="{{URL::route('painel.tipos.index', ['linha_id' => $linha->id])}}" class="btn btn-default">&larr; voltar</a>

      <hr>

      @include('painel.partials.mensagens')

      <table class="table table-striped table-bordered table-hover">

        <thead>
          <tr>
            <th>Título</th>
            <th><span class="glyphicon glyphicon-cog"></span></th>
          </tr>
        </thead>

        <tbody>
          @foreach ($tipo->produtos as $registro)

            <tr class="tr-row" id="row_{{ $registro->id }}">
              <td>{{ $registro->titulo_pt }}</td>
              <td class="crud-actions">
                <a href="{{ URL::route('painel.produtos.edit', $registro->id ) }}" class="btn btn-primary btn-sm">editar</a>

                <form action="{{ URL::route('painel.produtos.destroy', $registro->id) }}" method="post">
                  {!! csrf_field() !!}
                  <input type="hidden" name="_method" value="DELETE">
                  <button type="submit" class="btn btn-danger btn-sm btn-delete">excluir</button>
                </form>
              </td>
            </tr>
          @endforeach
        </tbody>

      </table>
    </div>
  </div>
</div>

@endsection
