@extends('painel.template')

@section('conteudo')

  <div class="container-fluid padded-bottom">
  	<div class="row">
  		<div class="col-xs-12 col-sm-8 col-md-6 col-lg-4">

        <h2>Linha: {{$registro->linha->titulo_pt}}</h2>

        <h2>Tipo: {{$registro->tipo->titulo_pt}}</h2>

        <h3>Editar Produto</h3>

        <hr>

        <a href="{{URL::route('painel.produtos.index', ['linha_id' => $registro->linhas_id, 'tipo_id' => $registro->tipos_id])}}" class="btn btn-default">&larr; voltar</a>

		    <hr>

		    @include('painel.partials.mensagens')

		    <form action="{{ URL::route('painel.produtos.update', $registro->id) }}" method="post" enctype="multipart/form-data">

					<input type="hidden" name="_method" value="PUT">

					{!! csrf_field() !!}

			  	<div class="form-group">
					  <label>Título em Português</label>
						<input type="text" class="form-control" name="titulo_pt" value="{{ $registro->titulo_pt }}" required>
					</div>
          <div class="form-group">
					  <label>Título em Inglês</label>
						<input type="text" class="form-control" name="titulo_en" value="{{ $registro->titulo_en }}">
					</div>
          <div class="form-group">
					  <label>Título em Espanhol</label>
						<input type="text" class="form-control" name="titulo_es" value="{{ $registro->titulo_es }}">
					</div>

					<hr>

          <div class="form-group">
            @if($registro->imagem)
              Imagem atual<br>
              <img src="assets/img/produtos/{{$registro->imagem}}"><br>
            @endif
            <label>Alterar Imagem</label>
            <input type="file" class="form-control" name="imagem">
          </div>

          <hr>

          <div class="form-group">
					  <label>Ingredientes em Português</label>
						<textarea class="form-control" name="ingredientes_pt">{{ $registro->ingredientes_pt }}</textarea>
					</div>
          <div class="form-group">
					  <label>Ingredientes em Inglês</label>
						<textarea class="form-control" name="ingredientes_en">{{ $registro->ingredientes_en }}</textarea>
					</div>
          <div class="form-group">
					  <label>Ingredientes em Espanhol</label>
						<textarea class="form-control" name="ingredientes_es">{{ $registro->ingredientes_es }}</textarea>
					</div>

          <hr>

          <div class="form-group">
					  <label>Porção para a tabela nutricional em Português</label>
						<input type="text" class="form-control" name="tabela_porcao_pt" value="{{ $registro->tabela_porcao_pt }}">
					</div>
          <div class="form-group">
					  <label>Porção para a tabela nutricional em Inglês</label>
						<input type="text" class="form-control" name="tabela_porcao_en" value="{{ $registro->tabela_porcao_en }}">
					</div>
          <div class="form-group">
					  <label>Porção para a tabela nutricional em Espanhol</label>
						<input type="text" class="form-control" name="tabela_porcao_es" value="{{ $registro->tabela_porcao_es }}">
					</div>

          <hr>

          <table class="tabela-nutricional">
            <thead>
              <tr>
                <td colspan="3">Tabela Nutricional</td>
              </tr>
              <tr>
                <td></td>
                <td>
                  Quantidade
                </td>
                <td>
                  % Valor Diário
                </td>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>Valor Calórico</td>
                <td>
                  <input type="text" name="nutri_valor_calorico" value="{{$registro->nutri_valor_calorico}}" class="form-control">
                </td>
                <td>
                  <input type="text" name="nutri_valor_calorico_pct" value="{{$registro->nutri_valor_calorico_pct}}" class="form-control">
                </td>
              </tr>
              <tr>
                <td>Carboidratos</td>
                <td>
                  <input type="text" name="nutri_carboidratos" value="{{$registro->nutri_carboidratos}}" class="form-control">
                </td>
                <td>
                  <input type="text" name="nutri_carboidratos_pct" value="{{$registro->nutri_carboidratos_pct}}" class="form-control">
                </td>
              </tr>
              <tr>
                <td>Proteínas</td>
                <td>
                  <input type="text" name="nutri_proteinas" value="{{$registro->nutri_proteinas}}" class="form-control">
                </td>
                <td>
                  <input type="text" name="nutri_proteinas_pct" value="{{$registro->nutri_proteinas_pct}}" class="form-control">
                </td>
              </tr>
              <tr>
                <td>Gorduras Totais</td>
                <td>
                  <input type="text" name="nutri_gorduras_totais" value="{{$registro->nutri_gorduras_totais}}" class="form-control">
                </td>
                <td>
                  <input type="text" name="nutri_gorduras_totais_pct" value="{{$registro->nutri_gorduras_totais_pct}}" class="form-control">
                </td>
              </tr>
              <tr>
                <td>Gorduras Saturadas</td>
                <td>
                  <input type="text" name="nutri_gorduras_saturadas" value="{{$registro->nutri_gorduras_saturadas}}" class="form-control">
                </td>
                <td>
                  <input type="text" name="nutri_gorduras_saturadas_pct" value="{{$registro->nutri_gorduras_saturadas_pct}}" class="form-control">
                </td>
              </tr>
              <tr>
                <td>Gorduras trans</td>
                <td>
                  <input type="text" name="nutri_gorduras_trans" value="{{$registro->nutri_gorduras_trans}}" class="form-control">
                </td>
                <td>
                  <input type="text" name="nutri_gorduras_trans_pct" value="{{$registro->nutri_gorduras_trans_pct}}" class="form-control">
                </td>
              </tr>
              <tr>
                <td>Fibra Alimentar</td>
                <td>
                  <input type="text" name="nutri_fibra_alimentar" value="{{$registro->nutri_fibra_alimentar}}" class="form-control">
                </td>
                <td>
                  <input type="text" name="nutri_fibra_alimentar_pct" value="{{$registro->nutri_fibra_alimentar_pct}}" class="form-control">
                </td>
              </tr>
              <tr>
                <td>Sódio</td>
                <td>
                  <input type="text" name="nutri_sodio" value="{{$registro->nutri_sodio}}" class="form-control">
                </td>
                <td>
                  <input type="text" name="nutri_sodio_pct" value="{{$registro->nutri_sodio_pct}}" class="form-control">
                </td>
              </tr>
            </tbody>
          </table>

          <hr>

					<button type="submit" title="Alterar" class="btn btn-success">Alterar</button>

					<a href="{{ URL::route('painel.produtos.index', ['linha_id' => $registro->linhas_id, 'tipo_id' => $registro->tipos_id])}}" title="Voltar" class="btn btn-default btn-voltar">Voltar</a>

				</form>

      </div>
		</div>
  </div>

@endsection
