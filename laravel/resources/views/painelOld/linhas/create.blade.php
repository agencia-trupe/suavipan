@extends('painel.template')

@section('conteudo')

  <div class="container-fluid padded-bottom">
  	<div class="row">
  		<div class="col-xs-12 col-sm-8 col-md-6 col-lg-4">

		    <h2>Editar Linha</h2>

		    <hr>

		    @include('painel.partials.mensagens')

		    <form action="{{ URL::route('painel.linhas.store')}}" method="post" enctype="multipart/form-data">

					<input type="hidden" name="_method" value="post">

					{!! csrf_field() !!}

			  	<div class="form-group">
					  <label>Título em Português</label>
						<input type="text" class="form-control" name="titulo_pt" required>
					</div>
          <div class="form-group">
					  <label>Título em Inglês</label>
						<input type="text" class="form-control" name="titulo_en" >
					</div>
          <div class="form-group">
					  <label>Título em Espanhol</label>
						<input type="text" class="form-control" name="titulo_es" >
					</div>

					<hr>

          <div class="form-group">
					  <label>Texto em Português</label>
						<textarea class="form-control" name="texto_pt"></textarea>
					</div>
          <div class="form-group">
					  <label>Texto em Inglês</label>
						<textarea class="form-control" name="texto_en"></textarea>
					</div>
          <div class="form-group">
					  <label>Texto em Espanhol</label>
						<textarea class="form-control" name="texto_es"></textarea>
					</div>

          <hr>

          <div class="form-group">
					  <label>Composição em Português</label>
						<textarea class="form-control textarea-simples" name="composicao_pt"></textarea>
					</div>
          <div class="form-group">
					  <label>Composição em Inglês</label>
						<textarea class="form-control textarea-simples" name="composicao_en"></textarea>
					</div>
          <div class="form-group">
					  <label>Composição em Espanhol</label>
						<textarea class="form-control textarea-simples" name="composicao_es"></textarea>
					</div>

          <hr>

          <div class="form-group">
					  <label>Cor</label>
						<input type="text" class="form-control" name="cor" >
					</div>

          <hr>

          <div class="form-group">  
            <label>imagem todos os produtos</label>
            <input type="file" class="form-control" name="imagem_todosprods">
          </div>

          <div class="form-group">
            <label>Alterar  Imagem - imagem todas as linhas</label>
            <input type="file" class="form-control" name="imagem_todaslinhas">
          </div>

          <div class="form-group">
            <label>Alterar  Imagem - imagem submenu</label>
            <input type="file" class="form-control" name="imagem_submenu">
          </div>

          <div class="form-group">
            <label>Alterar  Imagem - imagem home (ativo)</label>
            <input type="file" class="form-control" name="imagem_home_on">
          </div>

          <div class="form-group">
            <label>Alterar  Imagem - imagem home (inativo)</label>
            <input type="file" class="form-control" name="imagem_home_off">
          </div>

          <div class="form-group">
            <label>Alterar  Imagem - imagem de capa da linha</label>
            <input type="file" class="form-control" name="imagem_capa_linha">
          </div>

					<button type="submit" title="Alterar" class="btn btn-success">Criar</button>

					<a href="{{ URL::route('painel.linhas.index')}}" title="Voltar" class="btn btn-default btn-voltar">Voltar</a>

				</form>

      </div>
		</div>
  </div>

@endsection
