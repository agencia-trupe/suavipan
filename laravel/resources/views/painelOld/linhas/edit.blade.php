@extends('painel.template')

@section('conteudo')

  <div class="container-fluid padded-bottom">
  	<div class="row">
  		<div class="col-xs-12 col-sm-8 col-md-6 col-lg-4">

		    <h2>Editar Linha</h2>

		    <hr>

		    @include('painel.partials.mensagens')

		    <form action="{{ URL::route('painel.linhas.update', $registro->id) }}" method="post" enctype="multipart/form-data">

					<input type="hidden" name="_method" value="PUT">

					{!! csrf_field() !!}

			  	<div class="form-group">
					  <label>Título em Português</label>
						<input type="text" class="form-control" name="titulo_pt" value="{{ $registro->titulo_pt }}" required>
					</div>
          <div class="form-group">
					  <label>Título em Inglês</label>
						<input type="text" class="form-control" name="titulo_en" value="{{ $registro->titulo_en }}">
					</div>
          <div class="form-group">
					  <label>Título em Espanhol</label>
						<input type="text" class="form-control" name="titulo_es" value="{{ $registro->titulo_es }}">
					</div>

					<hr>

          <div class="form-group">
					  <label>Texto em Português</label>
						<textarea class="form-control" name="texto_pt">{{ $registro->texto_pt }}</textarea>
					</div>
          <div class="form-group">
					  <label>Texto em Inglês</label>
						<textarea class="form-control" name="texto_en">{{ $registro->texto_en }}</textarea>
					</div>
          <div class="form-group">
					  <label>Texto em Espanhol</label>
						<textarea class="form-control" name="texto_es">{{ $registro->texto_es }}</textarea>
					</div>

          <hr>

          <div class="form-group">
					  <label>Composição em Português</label>
						<textarea class="form-control textarea-simples" name="composicao_pt">{{ $registro->composicao_pt }}</textarea>
					</div>
          <div class="form-group">
					  <label>Composição em Inglês</label>
						<textarea class="form-control textarea-simples" name="composicao_en">{{ $registro->composicao_en }}</textarea>
					</div>
          <div class="form-group">
					  <label>Composição em Espanhol</label>
						<textarea class="form-control textarea-simples" name="composicao_es">{{ $registro->composicao_es }}</textarea>
					</div>

          <hr>

          <div class="form-group">
					  <label>Cor</label>
						<input type="text" class="form-control" name="cor" value="{{ $registro->cor }}">
					</div>

          <hr>

          <div class="form-group">
            @if($registro->imagem_todosprods)
              Imagem atual<br>
              <img src="assets/img/linhas/{{$registro->imagem_todosprods}}"><br>
            @endif
            <label>Alterar Imagem - imagem todos os produtos</label>
            <input type="file" class="form-control" name="imagem_todosprods">
          </div>

          <div class="form-group">
            @if($registro->imagem_todaslinhas)
              Imagem atual<br>
              <img src="assets/img/linhas/{{$registro->imagem_todaslinhas}}"><br>
            @endif
            <label>Alterar  Imagem - imagem todas as linhas</label>
            <input type="file" class="form-control" name="imagem_todaslinhas">
          </div>

          <div class="form-group">
            @if($registro->imagem_submenu)
              Imagem atual<br>
              <img src="assets/img/linhas/{{$registro->imagem_submenu}}"><br>
            @endif
            <label>Alterar  Imagem - imagem submenu</label>
            <input type="file" class="form-control" name="imagem_submenu">
          </div>

          <div class="form-group">
            @if($registro->imagem_home_on)
              Imagem atual<br>
              <img src="assets/img/linhas/{{$registro->imagem_home_on}}"><br>
            @endif
            <label>Alterar  Imagem - imagem home (ativo)</label>
            <input type="file" class="form-control" name="imagem_home_on">
          </div>

          <div class="form-group">
            @if($registro->imagem_home_off)
              Imagem atual<br>
              <img src="assets/img/linhas/{{$registro->imagem_home_off}}"><br>
            @endif
            <label>Alterar  Imagem - imagem home (inativo)</label>
            <input type="file" class="form-control" name="imagem_home_off">
          </div>

          <div class="form-group">
            @if($registro->imagem_capa_linha)
              Imagem atual<br>
              <img src="assets/img/linhas/{{$registro->imagem_capa_linha}}"><br>
            @endif
            <label>Alterar  Imagem - imagem de capa da linha</label>
            <input type="file" class="form-control" name="imagem_capa_linha">
          </div>

					<button type="submit" title="Alterar" class="btn btn-success">Alterar</button>

					<a href="{{ URL::route('painel.linhas.index')}}" title="Voltar" class="btn btn-default btn-voltar">Voltar</a>

				</form>

      </div>
		</div>
  </div>

@endsection
