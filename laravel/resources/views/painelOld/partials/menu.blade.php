<nav class="navbar navbar-default">
	<div class="container-fluid">

		<div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#menu-painel" aria-expanded="false">
			    <span class="sr-only">Navegação</span>
			    <span class="icon-bar"></span>
			    <span class="icon-bar"></span>
			    <span class="icon-bar"></span>
		    </button>
		    <a class="navbar-brand" title="Página Inicial" href="{{ URL::route('painel.dashboard') }}">Suavipan</a>
		</div>

		<div class="collapse navbar-collapse" id="menu-painel">
			<ul class="nav navbar-nav">

				<li @if(str_is('painel.dashboard*', Route::currentRouteName())) class="active" @endif>
					<a href="{{URL::route('painel.dashboard')}}" title="Início">Início</a>
				</li>

				<!--RESOURCEMENU-->

				<li @if(str_is('painel.conteudos*', Route::currentRouteName())) class="active" @endif>
					<a href="{{URL::route('painel.conteudos.index')}}" title="Conteúdos">Conteúdos</a>
				</li>

				<li @if(preg_match('/painel.(linhas*|tipos*|produtos*)/', Route::currentRouteName())) class="active" @endif>
					<a href="{{URL::route('painel.linhas.index')}}" title="Catálogo de Produtos">Catálogo de Produtos</a>
				</li>

				<li @if(preg_match('/painel.(cadastros*)/', Route::currentRouteName())) class="active" @endif>
					<a href="{{URL::route('painel.cadastros.index')}}" title="Cadastros">Cadastros</a>
				</li>

				<li class="dropdown @if(preg_match('~painel.(usuarios|idiomas*)~', Route::currentRouteName())) active @endif ">

					<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Sistema <b class="caret"></b></a>

					<ul class="dropdown-menu">
						<li @if(preg_match('~painel.(usuarios*)~', Route::currentRouteName())) class="active" @endif >
							<a href="{{URL::route('painel.usuarios.index')}}" title="Usuários do Painel">Usuários</a>
						</li>
						<li role="separator" class="divider"></li>
						<li>
							<a href="{{URL::route('painel.logout')}}" title="Logout">Logout</a>
						</li>
					</ul>
				</li>

			</ul>
		</div>

	</div>
</nav>
