<!DOCTYPE html>
<html lang="pt-BR" class="no-js">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta name="robots" content="index, nofollow" />
  <meta name="author" content="Trupe Design" />
  <meta name="copyright" content="2013 Trupe Design" />
  <meta name="viewport" content="width=device-width,initial-scale=1">

  <title>SuaviPan - Painel Administrativo</title>
	<meta name="description" content="">
  <meta name="keywords" content="" />

	<base href="{{ base_url() }}/">
	<script>var BASE = "{{ base_url() }}"</script>

	<link rel="stylesheet" href="assets/css/painel.css">

	<script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.0/jquery.min.js"></script>
	<script>window.jQuery || document.write('<script src="assets/vendor/jquery/dist/jquery.min.js"><\/script>')</script>

	<script src='assets/js/modernizr.js'></script>
</head>
	<body class="body-painel-login">

		<div class="main-painel-login">

			<div class="login">

				<form action="{{ URL::route('painel.auth') }}" method="post">

					{!! csrf_field() !!}

			  	<h3>SuaviPan <small>Painel Administrativo</small></h3>

			  	@if(count($errors) > 0)
				  	@foreach ($errors->all() as $error)
			      	<div class="alert alert-danger">{{ $error }}</div>
			      @endforeach
					@endif

					<div class="input-group">
						<span class="input-group-addon"><span class="glyphicon glyphicon-user"></span></span>
						<input type="text" class="form-control" placeholder="Usuário" value="{{ old('login') }}" required name="login" autofocus>
					</div>

					<div class="input-group">
						<span class="input-group-addon"><span class="glyphicon glyphicon-lock"></span></span>
						<input type="password" class="form-control" placeholder="Senha" required name="password">
					</div>

					<div class="submit-placeholder">
						<label>
							<input type="checkbox" value="1" name="lembrar"> Lembrar de mim
						</label>
			   			<input type="submit" class="btn btn-primary" value="Login">
					</div>

				</form>

			</div>

		</div>

		<footer>
			<div class="container">
				<a href="http://www.trupe.net" target="_blank" title="Criação de Sites : Trupe Agência Criativa">© Criação de Sites : Trupe Agência Criativa</a>
			</div>
		</footer>

		<script src='assets/js/bootstrap.js'></script>

	</body>
</html>
